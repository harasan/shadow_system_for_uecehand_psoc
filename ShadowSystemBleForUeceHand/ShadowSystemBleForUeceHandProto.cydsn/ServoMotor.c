/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "ServoMotor.h"

#define PULSE_WIDTH_PER_POSITION 11.7647
#define MIN_PULSE_WIDTH 3000
#define DEFAULT_SPEED 100
#define LFCLK_FREQUENCY 32.768e3f

static uint16 motorSpeeds[NUM_OF_SERVO_MOTOR];
static uint16 destPulseWidth[NUM_OF_SERVO_MOTOR];
static bool isMoving[NUM_OF_SERVO_MOTOR];
static float positionUpdateFrequency;

static uint8 PulseWidthToPosition(uint16 pulseWidth);
static uint16 PositionToPulseWidth(uint8 position);
static uint16 ServoPwmReadCompare(uint8 motorId);
static void ServoPwmWriteCompare(uint8 motorId, uint16 compare);
static CY_ISR_PROTO(UpdateMotorPosition);

void InitServoMotor(void)
{
    ServoPwm1_Start();
    ServoPwm2_Start();
    ServoPwm3_Start();
    ServoPwm4_Start();
    
    for(uint8 i = 1; i <= NUM_OF_SERVO_MOTOR; i++){
        SetServoMotorSpeed(i, DEFAULT_SPEED);
        ServoPwmWriteCompare(i, 0);
    }
    positionUpdateFrequency = LFCLK_FREQUENCY / CySysWdtGetMatch(CY_SYS_WDT_COUNTER0);
    CySysWdtSetInterruptCallback(CY_SYS_WDT_COUNTER0, UpdateMotorPosition);
}

void SetServoMotorSpeed(uint8 motorId, uint16 positionPerSecond)
{
    uint16 speed = PositionToPulseWidth(positionPerSecond / positionUpdateFrequency);
    motorSpeeds[motorId - 1] = speed;
}

uint16 GetServoMotorSpeed(uint8 motorId)
{
    return PulseWidthToPosition(motorSpeeds[motorId - 1]) * positionUpdateFrequency;
}

uint8 GetServoMotorPosition(uint8 motorId)
{
    return PulseWidthToPosition(ServoPwmReadCompare(motorId));
}

void SetServoMotorPosition(uint8 motorId, uint8 position)
{
    uint16 pulseWidth = PositionToPulseWidth(position);
    ServoPwmWriteCompare(motorId, pulseWidth);
    destPulseWidth[motorId - 1] = pulseWidth;
}

void KeepCurrentServoMotorPosition(uint8 motorId)
{
    destPulseWidth[motorId - 1] = ServoPwmReadCompare(motorId);
}

void StopServoMotor(uint8 motorId)
{
    uint16 pulseWidth = 0;
    ServoPwmWriteCompare(motorId, pulseWidth);
    destPulseWidth[motorId - 1] = pulseWidth;
}

void SetServoMotorPositionWithSpeedLimit(uint8 motorId, uint8 position)
{
    destPulseWidth[motorId] = PositionToPulseWidth(position);
}

bool GetIsServoMotorMoving(uint8 motorId)
{
    return isMoving[motorId];
}

static uint8 PulseWidthToPosition(uint16 pulseWidth)
{
    if(pulseWidth < MIN_PULSE_WIDTH - 1)
    {
        return 0;
    }
    
    pulseWidth -= MIN_PULSE_WIDTH - 1;
    return (uint8)(pulseWidth / PULSE_WIDTH_PER_POSITION);
}

static uint16 PositionToPulseWidth(uint8 position)
{
    return (uint16)(PULSE_WIDTH_PER_POSITION * position + MIN_PULSE_WIDTH) - 1;
}

static uint16 ServoPwmReadCompare(uint8 motorId)
{
    uint16 compare = 0;
    switch(motorId)
    {
        case 1:
            compare = ServoPwm1_ReadCompare();
            break;
        case 2:
            compare = ServoPwm2_ReadCompare();
            break;
        case 3:
            compare = ServoPwm3_ReadCompare();
            break;
        case 4:
            compare = ServoPwm4_ReadCompare();
            break;
    }
    return compare;
}

static void ServoPwmWriteCompare(uint8 motorId, uint16 compare)
{
    switch(motorId)
    {
        case 1:
            ServoPwm1_WriteCompare(compare);
            break;
        case 2:
            ServoPwm2_WriteCompare(compare);
            break;
        case 3:
            ServoPwm3_WriteCompare(compare);
            break;
        case 4:
            ServoPwm4_WriteCompare(compare);
            break;
    }
}

static CY_ISR(UpdateMotorPosition)
{
    uint16 pulseWidth[NUM_OF_SERVO_MOTOR];
    for(uint8 i = 0; i < NUM_OF_SERVO_MOTOR; i++)
    {
        pulseWidth[i] = ServoPwmReadCompare(i + 1);
    }
    
    for(uint8 i = 0; i < NUM_OF_SERVO_MOTOR; i++)
    {
        if(pulseWidth[i] == destPulseWidth[i])
        {
            isMoving[i] = false;
            continue;
        }
        
        uint16 tmp = pulseWidth[i];
        if(tmp < destPulseWidth[i])
        {
            tmp += motorSpeeds[i];
            tmp = tmp > destPulseWidth[i]? destPulseWidth[i] : tmp;
        }
        else
        {
            tmp -= motorSpeeds[i];
            tmp = tmp < destPulseWidth[i]? destPulseWidth[i] : tmp;
        }
        pulseWidth[i] = tmp;
        isMoving[i] = true;
    }
    
    for(uint8 i = 0; i < NUM_OF_SERVO_MOTOR; i++)
    {
        ServoPwmWriteCompare(i + 1, pulseWidth[i]);
    }
}

/* [] END OF FILE */
