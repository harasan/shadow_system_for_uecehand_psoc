/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include <stdbool.h>

#ifndef ADC_H
    #define ADC_H
    
    //ADCのチャンネル数
    #define ADC_CHANNEL_NUMBER  3
    //1パケットに含まれるサンプル数
    #define ADC_SAMPLE_COUNT    40
    //ADCのビット数
    #define ADC_RESOLUTION      12
    
    extern int16 adcSamplesBuffer[ADC_CHANNEL_NUMBER][ADC_SAMPLE_COUNT];
    
    void InitAdc(void);
    void StartAdc(void);
    void StopAdc(void);
    bool GetIsAdcSamplesReady(void);
    void ResetIsAdcSamplesReady(void);
    
#endif
/* [] END OF FILE */
