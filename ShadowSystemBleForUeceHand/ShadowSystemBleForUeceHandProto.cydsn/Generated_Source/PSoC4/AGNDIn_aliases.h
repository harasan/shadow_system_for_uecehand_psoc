/*******************************************************************************
* File Name: AGNDIn.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_AGNDIn_ALIASES_H) /* Pins AGNDIn_ALIASES_H */
#define CY_PINS_AGNDIn_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define AGNDIn_0			(AGNDIn__0__PC)
#define AGNDIn_0_PS		(AGNDIn__0__PS)
#define AGNDIn_0_PC		(AGNDIn__0__PC)
#define AGNDIn_0_DR		(AGNDIn__0__DR)
#define AGNDIn_0_SHIFT	(AGNDIn__0__SHIFT)
#define AGNDIn_0_INTR	((uint16)((uint16)0x0003u << (AGNDIn__0__SHIFT*2u)))

#define AGNDIn_INTR_ALL	 ((uint16)(AGNDIn_0_INTR))


#endif /* End Pins AGNDIn_ALIASES_H */


/* [] END OF FILE */
