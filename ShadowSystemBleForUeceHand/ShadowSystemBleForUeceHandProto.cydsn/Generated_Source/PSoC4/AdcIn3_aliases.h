/*******************************************************************************
* File Name: AdcIn3.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_AdcIn3_ALIASES_H) /* Pins AdcIn3_ALIASES_H */
#define CY_PINS_AdcIn3_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define AdcIn3_0			(AdcIn3__0__PC)
#define AdcIn3_0_PS		(AdcIn3__0__PS)
#define AdcIn3_0_PC		(AdcIn3__0__PC)
#define AdcIn3_0_DR		(AdcIn3__0__DR)
#define AdcIn3_0_SHIFT	(AdcIn3__0__SHIFT)
#define AdcIn3_0_INTR	((uint16)((uint16)0x0003u << (AdcIn3__0__SHIFT*2u)))

#define AdcIn3_INTR_ALL	 ((uint16)(AdcIn3_0_INTR))


#endif /* End Pins AdcIn3_ALIASES_H */


/* [] END OF FILE */
