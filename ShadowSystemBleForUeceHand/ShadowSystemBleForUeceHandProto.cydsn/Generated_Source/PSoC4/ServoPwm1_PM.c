/*******************************************************************************
* File Name: ServoPwm1_PM.c
* Version 2.10
*
* Description:
*  This file contains the setup, control, and status commands to support
*  the component operations in the low power mode.
*
* Note:
*  None
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "ServoPwm1.h"

static ServoPwm1_BACKUP_STRUCT ServoPwm1_backup;


/*******************************************************************************
* Function Name: ServoPwm1_SaveConfig
********************************************************************************
*
* Summary:
*  All configuration registers are retention. Nothing to save here.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm1_SaveConfig(void)
{

}


/*******************************************************************************
* Function Name: ServoPwm1_Sleep
********************************************************************************
*
* Summary:
*  Stops the component operation and saves the user configuration.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm1_Sleep(void)
{
    if(0u != (ServoPwm1_BLOCK_CONTROL_REG & ServoPwm1_MASK))
    {
        ServoPwm1_backup.enableState = 1u;
    }
    else
    {
        ServoPwm1_backup.enableState = 0u;
    }

    ServoPwm1_Stop();
    ServoPwm1_SaveConfig();
}


/*******************************************************************************
* Function Name: ServoPwm1_RestoreConfig
********************************************************************************
*
* Summary:
*  All configuration registers are retention. Nothing to restore here.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm1_RestoreConfig(void)
{

}


/*******************************************************************************
* Function Name: ServoPwm1_Wakeup
********************************************************************************
*
* Summary:
*  Restores the user configuration and restores the enable state.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm1_Wakeup(void)
{
    ServoPwm1_RestoreConfig();

    if(0u != ServoPwm1_backup.enableState)
    {
        ServoPwm1_Enable();
    }
}


/* [] END OF FILE */
