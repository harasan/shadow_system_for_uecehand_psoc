/*******************************************************************************
* File Name: ServoPwm4_PM.c
* Version 2.10
*
* Description:
*  This file contains the setup, control, and status commands to support
*  the component operations in the low power mode.
*
* Note:
*  None
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "ServoPwm4.h"

static ServoPwm4_BACKUP_STRUCT ServoPwm4_backup;


/*******************************************************************************
* Function Name: ServoPwm4_SaveConfig
********************************************************************************
*
* Summary:
*  All configuration registers are retention. Nothing to save here.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm4_SaveConfig(void)
{

}


/*******************************************************************************
* Function Name: ServoPwm4_Sleep
********************************************************************************
*
* Summary:
*  Stops the component operation and saves the user configuration.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm4_Sleep(void)
{
    if(0u != (ServoPwm4_BLOCK_CONTROL_REG & ServoPwm4_MASK))
    {
        ServoPwm4_backup.enableState = 1u;
    }
    else
    {
        ServoPwm4_backup.enableState = 0u;
    }

    ServoPwm4_Stop();
    ServoPwm4_SaveConfig();
}


/*******************************************************************************
* Function Name: ServoPwm4_RestoreConfig
********************************************************************************
*
* Summary:
*  All configuration registers are retention. Nothing to restore here.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm4_RestoreConfig(void)
{

}


/*******************************************************************************
* Function Name: ServoPwm4_Wakeup
********************************************************************************
*
* Summary:
*  Restores the user configuration and restores the enable state.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm4_Wakeup(void)
{
    ServoPwm4_RestoreConfig();

    if(0u != ServoPwm4_backup.enableState)
    {
        ServoPwm4_Enable();
    }
}


/* [] END OF FILE */
