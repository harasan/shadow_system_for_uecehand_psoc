/*******************************************************************************
* File Name: AdcTrigger.h
* Version 3.30
*
* Description:
*  Contains the prototypes and constants for the functions available to the
*  PWM user module.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PWM_AdcTrigger_H)
#define CY_PWM_AdcTrigger_H

#include "cyfitter.h"
#include "cytypes.h"
#include "CyLib.h" /* For CyEnterCriticalSection() and CyExitCriticalSection() functions */

extern uint8 AdcTrigger_initVar;


/***************************************
* Conditional Compilation Parameters
***************************************/
#define AdcTrigger_Resolution                     (8u)
#define AdcTrigger_UsingFixedFunction             (0u)
#define AdcTrigger_DeadBandMode                   (0u)
#define AdcTrigger_KillModeMinTime                (0u)
#define AdcTrigger_KillMode                       (0u)
#define AdcTrigger_PWMMode                        (0u)
#define AdcTrigger_PWMModeIsCenterAligned         (0u)
#define AdcTrigger_DeadBandUsed                   (0u)
#define AdcTrigger_DeadBand2_4                    (0u)

#if !defined(AdcTrigger_PWMUDB_genblk8_stsreg__REMOVED)
    #define AdcTrigger_UseStatus                  (1u)
#else
    #define AdcTrigger_UseStatus                  (0u)
#endif /* !defined(AdcTrigger_PWMUDB_genblk8_stsreg__REMOVED) */

#if !defined(AdcTrigger_PWMUDB_genblk1_ctrlreg__REMOVED)
    #define AdcTrigger_UseControl                 (1u)
#else
    #define AdcTrigger_UseControl                 (0u)
#endif /* !defined(AdcTrigger_PWMUDB_genblk1_ctrlreg__REMOVED) */

#define AdcTrigger_UseOneCompareMode              (1u)
#define AdcTrigger_MinimumKillTime                (1u)
#define AdcTrigger_EnableMode                     (0u)

#define AdcTrigger_CompareMode1SW                 (0u)
#define AdcTrigger_CompareMode2SW                 (0u)

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component PWM_v3_30 requires cy_boot v3.0 or later
#endif /* (CY_ PSOC5LP) */

/* Use Kill Mode Enumerated Types */
#define AdcTrigger__B_PWM__DISABLED 0
#define AdcTrigger__B_PWM__ASYNCHRONOUS 1
#define AdcTrigger__B_PWM__SINGLECYCLE 2
#define AdcTrigger__B_PWM__LATCHED 3
#define AdcTrigger__B_PWM__MINTIME 4


/* Use Dead Band Mode Enumerated Types */
#define AdcTrigger__B_PWM__DBMDISABLED 0
#define AdcTrigger__B_PWM__DBM_2_4_CLOCKS 1
#define AdcTrigger__B_PWM__DBM_256_CLOCKS 2


/* Used PWM Mode Enumerated Types */
#define AdcTrigger__B_PWM__ONE_OUTPUT 0
#define AdcTrigger__B_PWM__TWO_OUTPUTS 1
#define AdcTrigger__B_PWM__DUAL_EDGE 2
#define AdcTrigger__B_PWM__CENTER_ALIGN 3
#define AdcTrigger__B_PWM__DITHER 5
#define AdcTrigger__B_PWM__HARDWARESELECT 4


/* Used PWM Compare Mode Enumerated Types */
#define AdcTrigger__B_PWM__LESS_THAN 1
#define AdcTrigger__B_PWM__LESS_THAN_OR_EQUAL 2
#define AdcTrigger__B_PWM__GREATER_THAN 3
#define AdcTrigger__B_PWM__GREATER_THAN_OR_EQUAL_TO 4
#define AdcTrigger__B_PWM__EQUAL 0
#define AdcTrigger__B_PWM__FIRMWARE 5



/***************************************
* Data Struct Definition
***************************************/


/**************************************************************************
 * Sleep Wakeup Backup structure for PWM Component
 *************************************************************************/
typedef struct
{

    uint8 PWMEnableState;

    #if(!AdcTrigger_UsingFixedFunction)
        uint8 PWMUdb;               /* PWM Current Counter value  */
        #if(!AdcTrigger_PWMModeIsCenterAligned)
            uint8 PWMPeriod;
        #endif /* (!AdcTrigger_PWMModeIsCenterAligned) */
        #if (AdcTrigger_UseStatus)
            uint8 InterruptMaskValue;   /* PWM Current Interrupt Mask */
        #endif /* (AdcTrigger_UseStatus) */

        /* Backup for Deadband parameters */
        #if(AdcTrigger_DeadBandMode == AdcTrigger__B_PWM__DBM_256_CLOCKS || \
            AdcTrigger_DeadBandMode == AdcTrigger__B_PWM__DBM_2_4_CLOCKS)
            uint8 PWMdeadBandValue; /* Dead Band Counter Current Value */
        #endif /* deadband count is either 2-4 clocks or 256 clocks */

        /* Backup Kill Mode Counter*/
        #if(AdcTrigger_KillModeMinTime)
            uint8 PWMKillCounterPeriod; /* Kill Mode period value */
        #endif /* (AdcTrigger_KillModeMinTime) */

        /* Backup control register */
        #if(AdcTrigger_UseControl)
            uint8 PWMControlRegister; /* PWM Control Register value */
        #endif /* (AdcTrigger_UseControl) */

    #endif /* (!AdcTrigger_UsingFixedFunction) */

}AdcTrigger_backupStruct;


/***************************************
*        Function Prototypes
 **************************************/

void    AdcTrigger_Start(void) ;
void    AdcTrigger_Stop(void) ;

#if (AdcTrigger_UseStatus || AdcTrigger_UsingFixedFunction)
    void  AdcTrigger_SetInterruptMode(uint8 interruptMode) ;
    uint8 AdcTrigger_ReadStatusRegister(void) ;
#endif /* (AdcTrigger_UseStatus || AdcTrigger_UsingFixedFunction) */

#define AdcTrigger_GetInterruptSource() AdcTrigger_ReadStatusRegister()

#if (AdcTrigger_UseControl)
    uint8 AdcTrigger_ReadControlRegister(void) ;
    void  AdcTrigger_WriteControlRegister(uint8 control)
          ;
#endif /* (AdcTrigger_UseControl) */

#if (AdcTrigger_UseOneCompareMode)
   #if (AdcTrigger_CompareMode1SW)
       void    AdcTrigger_SetCompareMode(uint8 comparemode)
               ;
   #endif /* (AdcTrigger_CompareMode1SW) */
#else
    #if (AdcTrigger_CompareMode1SW)
        void    AdcTrigger_SetCompareMode1(uint8 comparemode)
                ;
    #endif /* (AdcTrigger_CompareMode1SW) */
    #if (AdcTrigger_CompareMode2SW)
        void    AdcTrigger_SetCompareMode2(uint8 comparemode)
                ;
    #endif /* (AdcTrigger_CompareMode2SW) */
#endif /* (AdcTrigger_UseOneCompareMode) */

#if (!AdcTrigger_UsingFixedFunction)
    uint8   AdcTrigger_ReadCounter(void) ;
    uint8 AdcTrigger_ReadCapture(void) ;

    #if (AdcTrigger_UseStatus)
            void AdcTrigger_ClearFIFO(void) ;
    #endif /* (AdcTrigger_UseStatus) */

    void    AdcTrigger_WriteCounter(uint8 counter)
            ;
#endif /* (!AdcTrigger_UsingFixedFunction) */

void    AdcTrigger_WritePeriod(uint8 period)
        ;
uint8 AdcTrigger_ReadPeriod(void) ;

#if (AdcTrigger_UseOneCompareMode)
    void    AdcTrigger_WriteCompare(uint8 compare)
            ;
    uint8 AdcTrigger_ReadCompare(void) ;
#else
    void    AdcTrigger_WriteCompare1(uint8 compare)
            ;
    uint8 AdcTrigger_ReadCompare1(void) ;
    void    AdcTrigger_WriteCompare2(uint8 compare)
            ;
    uint8 AdcTrigger_ReadCompare2(void) ;
#endif /* (AdcTrigger_UseOneCompareMode) */


#if (AdcTrigger_DeadBandUsed)
    void    AdcTrigger_WriteDeadTime(uint8 deadtime) ;
    uint8   AdcTrigger_ReadDeadTime(void) ;
#endif /* (AdcTrigger_DeadBandUsed) */

#if ( AdcTrigger_KillModeMinTime)
    void AdcTrigger_WriteKillTime(uint8 killtime) ;
    uint8 AdcTrigger_ReadKillTime(void) ;
#endif /* ( AdcTrigger_KillModeMinTime) */

void AdcTrigger_Init(void) ;
void AdcTrigger_Enable(void) ;
void AdcTrigger_Sleep(void) ;
void AdcTrigger_Wakeup(void) ;
void AdcTrigger_SaveConfig(void) ;
void AdcTrigger_RestoreConfig(void) ;


/***************************************
*         Initialization Values
**************************************/
#define AdcTrigger_INIT_PERIOD_VALUE          (199u)
#define AdcTrigger_INIT_COMPARE_VALUE1        (99u)
#define AdcTrigger_INIT_COMPARE_VALUE2        (63u)
#define AdcTrigger_INIT_INTERRUPTS_MODE       (uint8)(((uint8)(0u <<   \
                                                    AdcTrigger_STATUS_TC_INT_EN_MASK_SHIFT)) | \
                                                    (uint8)((uint8)(0u <<  \
                                                    AdcTrigger_STATUS_CMP2_INT_EN_MASK_SHIFT)) | \
                                                    (uint8)((uint8)(0u <<  \
                                                    AdcTrigger_STATUS_CMP1_INT_EN_MASK_SHIFT )) | \
                                                    (uint8)((uint8)(0u <<  \
                                                    AdcTrigger_STATUS_KILL_INT_EN_MASK_SHIFT )))
#define AdcTrigger_DEFAULT_COMPARE2_MODE      (uint8)((uint8)1u <<  AdcTrigger_CTRL_CMPMODE2_SHIFT)
#define AdcTrigger_DEFAULT_COMPARE1_MODE      (uint8)((uint8)4u <<  AdcTrigger_CTRL_CMPMODE1_SHIFT)
#define AdcTrigger_INIT_DEAD_TIME             (1u)


/********************************
*         Registers
******************************** */

#if (AdcTrigger_UsingFixedFunction)
   #define AdcTrigger_PERIOD_LSB              (*(reg16 *) AdcTrigger_PWMHW__PER0)
   #define AdcTrigger_PERIOD_LSB_PTR          ( (reg16 *) AdcTrigger_PWMHW__PER0)
   #define AdcTrigger_COMPARE1_LSB            (*(reg16 *) AdcTrigger_PWMHW__CNT_CMP0)
   #define AdcTrigger_COMPARE1_LSB_PTR        ( (reg16 *) AdcTrigger_PWMHW__CNT_CMP0)
   #define AdcTrigger_COMPARE2_LSB            (0x00u)
   #define AdcTrigger_COMPARE2_LSB_PTR        (0x00u)
   #define AdcTrigger_COUNTER_LSB             (*(reg16 *) AdcTrigger_PWMHW__CNT_CMP0)
   #define AdcTrigger_COUNTER_LSB_PTR         ( (reg16 *) AdcTrigger_PWMHW__CNT_CMP0)
   #define AdcTrigger_CAPTURE_LSB             (*(reg16 *) AdcTrigger_PWMHW__CAP0)
   #define AdcTrigger_CAPTURE_LSB_PTR         ( (reg16 *) AdcTrigger_PWMHW__CAP0)
   #define AdcTrigger_RT1                     (*(reg8 *)  AdcTrigger_PWMHW__RT1)
   #define AdcTrigger_RT1_PTR                 ( (reg8 *)  AdcTrigger_PWMHW__RT1)

#else
   #if (AdcTrigger_Resolution == 8u) /* 8bit - PWM */

       #if(AdcTrigger_PWMModeIsCenterAligned)
           #define AdcTrigger_PERIOD_LSB      (*(reg8 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__D1_REG)
           #define AdcTrigger_PERIOD_LSB_PTR  ((reg8 *)   AdcTrigger_PWMUDB_sP8_pwmdp_u0__D1_REG)
       #else
           #define AdcTrigger_PERIOD_LSB      (*(reg8 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__F0_REG)
           #define AdcTrigger_PERIOD_LSB_PTR  ((reg8 *)   AdcTrigger_PWMUDB_sP8_pwmdp_u0__F0_REG)
       #endif /* (AdcTrigger_PWMModeIsCenterAligned) */

       #define AdcTrigger_COMPARE1_LSB        (*(reg8 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__D0_REG)
       #define AdcTrigger_COMPARE1_LSB_PTR    ((reg8 *)   AdcTrigger_PWMUDB_sP8_pwmdp_u0__D0_REG)
       #define AdcTrigger_COMPARE2_LSB        (*(reg8 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__D1_REG)
       #define AdcTrigger_COMPARE2_LSB_PTR    ((reg8 *)   AdcTrigger_PWMUDB_sP8_pwmdp_u0__D1_REG)
       #define AdcTrigger_COUNTERCAP_LSB      (*(reg8 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__A1_REG)
       #define AdcTrigger_COUNTERCAP_LSB_PTR  ((reg8 *)   AdcTrigger_PWMUDB_sP8_pwmdp_u0__A1_REG)
       #define AdcTrigger_COUNTER_LSB         (*(reg8 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__A0_REG)
       #define AdcTrigger_COUNTER_LSB_PTR     ((reg8 *)   AdcTrigger_PWMUDB_sP8_pwmdp_u0__A0_REG)
       #define AdcTrigger_CAPTURE_LSB         (*(reg8 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__F1_REG)
       #define AdcTrigger_CAPTURE_LSB_PTR     ((reg8 *)   AdcTrigger_PWMUDB_sP8_pwmdp_u0__F1_REG)

   #else
        #if(CY_PSOC3) /* 8-bit address space */
            #if(AdcTrigger_PWMModeIsCenterAligned)
               #define AdcTrigger_PERIOD_LSB      (*(reg16 *) AdcTrigger_PWMUDB_sP8_pwmdp_u0__D1_REG)
               #define AdcTrigger_PERIOD_LSB_PTR  ((reg16 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__D1_REG)
            #else
               #define AdcTrigger_PERIOD_LSB      (*(reg16 *) AdcTrigger_PWMUDB_sP8_pwmdp_u0__F0_REG)
               #define AdcTrigger_PERIOD_LSB_PTR  ((reg16 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__F0_REG)
            #endif /* (AdcTrigger_PWMModeIsCenterAligned) */

            #define AdcTrigger_COMPARE1_LSB       (*(reg16 *) AdcTrigger_PWMUDB_sP8_pwmdp_u0__D0_REG)
            #define AdcTrigger_COMPARE1_LSB_PTR   ((reg16 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__D0_REG)
            #define AdcTrigger_COMPARE2_LSB       (*(reg16 *) AdcTrigger_PWMUDB_sP8_pwmdp_u0__D1_REG)
            #define AdcTrigger_COMPARE2_LSB_PTR   ((reg16 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__D1_REG)
            #define AdcTrigger_COUNTERCAP_LSB     (*(reg16 *) AdcTrigger_PWMUDB_sP8_pwmdp_u0__A1_REG)
            #define AdcTrigger_COUNTERCAP_LSB_PTR ((reg16 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__A1_REG)
            #define AdcTrigger_COUNTER_LSB        (*(reg16 *) AdcTrigger_PWMUDB_sP8_pwmdp_u0__A0_REG)
            #define AdcTrigger_COUNTER_LSB_PTR    ((reg16 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__A0_REG)
            #define AdcTrigger_CAPTURE_LSB        (*(reg16 *) AdcTrigger_PWMUDB_sP8_pwmdp_u0__F1_REG)
            #define AdcTrigger_CAPTURE_LSB_PTR    ((reg16 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__F1_REG)
        #else
            #if(AdcTrigger_PWMModeIsCenterAligned)
               #define AdcTrigger_PERIOD_LSB      (*(reg16 *) AdcTrigger_PWMUDB_sP8_pwmdp_u0__16BIT_D1_REG)
               #define AdcTrigger_PERIOD_LSB_PTR  ((reg16 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__16BIT_D1_REG)
            #else
               #define AdcTrigger_PERIOD_LSB      (*(reg16 *) AdcTrigger_PWMUDB_sP8_pwmdp_u0__16BIT_F0_REG)
               #define AdcTrigger_PERIOD_LSB_PTR  ((reg16 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__16BIT_F0_REG)
            #endif /* (AdcTrigger_PWMModeIsCenterAligned) */

            #define AdcTrigger_COMPARE1_LSB       (*(reg16 *) AdcTrigger_PWMUDB_sP8_pwmdp_u0__16BIT_D0_REG)
            #define AdcTrigger_COMPARE1_LSB_PTR   ((reg16 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__16BIT_D0_REG)
            #define AdcTrigger_COMPARE2_LSB       (*(reg16 *) AdcTrigger_PWMUDB_sP8_pwmdp_u0__16BIT_D1_REG)
            #define AdcTrigger_COMPARE2_LSB_PTR   ((reg16 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__16BIT_D1_REG)
            #define AdcTrigger_COUNTERCAP_LSB     (*(reg16 *) AdcTrigger_PWMUDB_sP8_pwmdp_u0__16BIT_A1_REG)
            #define AdcTrigger_COUNTERCAP_LSB_PTR ((reg16 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__16BIT_A1_REG)
            #define AdcTrigger_COUNTER_LSB        (*(reg16 *) AdcTrigger_PWMUDB_sP8_pwmdp_u0__16BIT_A0_REG)
            #define AdcTrigger_COUNTER_LSB_PTR    ((reg16 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__16BIT_A0_REG)
            #define AdcTrigger_CAPTURE_LSB        (*(reg16 *) AdcTrigger_PWMUDB_sP8_pwmdp_u0__16BIT_F1_REG)
            #define AdcTrigger_CAPTURE_LSB_PTR    ((reg16 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__16BIT_F1_REG)
        #endif /* (CY_PSOC3) */

       #define AdcTrigger_AUX_CONTROLDP1          (*(reg8 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u1__DP_AUX_CTL_REG)
       #define AdcTrigger_AUX_CONTROLDP1_PTR      ((reg8 *)   AdcTrigger_PWMUDB_sP8_pwmdp_u1__DP_AUX_CTL_REG)

   #endif /* (AdcTrigger_Resolution == 8) */

   #define AdcTrigger_COUNTERCAP_LSB_PTR_8BIT ( (reg8 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__A1_REG)
   #define AdcTrigger_AUX_CONTROLDP0          (*(reg8 *)  AdcTrigger_PWMUDB_sP8_pwmdp_u0__DP_AUX_CTL_REG)
   #define AdcTrigger_AUX_CONTROLDP0_PTR      ((reg8 *)   AdcTrigger_PWMUDB_sP8_pwmdp_u0__DP_AUX_CTL_REG)

#endif /* (AdcTrigger_UsingFixedFunction) */

#if(AdcTrigger_KillModeMinTime )
    #define AdcTrigger_KILLMODEMINTIME        (*(reg8 *)  AdcTrigger_PWMUDB_sKM_killmodecounterdp_u0__D0_REG)
    #define AdcTrigger_KILLMODEMINTIME_PTR    ((reg8 *)   AdcTrigger_PWMUDB_sKM_killmodecounterdp_u0__D0_REG)
    /* Fixed Function Block has no Kill Mode parameters because it is Asynchronous only */
#endif /* (AdcTrigger_KillModeMinTime ) */

#if(AdcTrigger_DeadBandMode == AdcTrigger__B_PWM__DBM_256_CLOCKS)
    #define AdcTrigger_DEADBAND_COUNT         (*(reg8 *)  AdcTrigger_PWMUDB_sDB255_deadbandcounterdp_u0__D0_REG)
    #define AdcTrigger_DEADBAND_COUNT_PTR     ((reg8 *)   AdcTrigger_PWMUDB_sDB255_deadbandcounterdp_u0__D0_REG)
    #define AdcTrigger_DEADBAND_LSB_PTR       ((reg8 *)   AdcTrigger_PWMUDB_sDB255_deadbandcounterdp_u0__A0_REG)
    #define AdcTrigger_DEADBAND_LSB           (*(reg8 *)  AdcTrigger_PWMUDB_sDB255_deadbandcounterdp_u0__A0_REG)
#elif(AdcTrigger_DeadBandMode == AdcTrigger__B_PWM__DBM_2_4_CLOCKS)
    
    /* In Fixed Function Block these bits are in the control blocks control register */
    #if (AdcTrigger_UsingFixedFunction)
        #define AdcTrigger_DEADBAND_COUNT         (*(reg8 *)  AdcTrigger_PWMHW__CFG0)
        #define AdcTrigger_DEADBAND_COUNT_PTR     ((reg8 *)   AdcTrigger_PWMHW__CFG0)
        #define AdcTrigger_DEADBAND_COUNT_MASK    (uint8)((uint8)0x03u << AdcTrigger_DEADBAND_COUNT_SHIFT)

        /* As defined by the Register Map as DEADBAND_PERIOD[1:0] in CFG0 */
        #define AdcTrigger_DEADBAND_COUNT_SHIFT   (0x06u)
    #else
        /* Lower two bits of the added control register define the count 1-3 */
        #define AdcTrigger_DEADBAND_COUNT         (*(reg8 *)  AdcTrigger_PWMUDB_genblk7_dbctrlreg__CONTROL_REG)
        #define AdcTrigger_DEADBAND_COUNT_PTR     ((reg8 *)   AdcTrigger_PWMUDB_genblk7_dbctrlreg__CONTROL_REG)
        #define AdcTrigger_DEADBAND_COUNT_MASK    (uint8)((uint8)0x03u << AdcTrigger_DEADBAND_COUNT_SHIFT)

        /* As defined by the verilog implementation of the Control Register */
        #define AdcTrigger_DEADBAND_COUNT_SHIFT   (0x00u)
    #endif /* (AdcTrigger_UsingFixedFunction) */
#endif /* (AdcTrigger_DeadBandMode == AdcTrigger__B_PWM__DBM_256_CLOCKS) */



#if (AdcTrigger_UsingFixedFunction)
    #define AdcTrigger_STATUS                 (*(reg8 *) AdcTrigger_PWMHW__SR0)
    #define AdcTrigger_STATUS_PTR             ((reg8 *) AdcTrigger_PWMHW__SR0)
    #define AdcTrigger_STATUS_MASK            (*(reg8 *) AdcTrigger_PWMHW__SR0)
    #define AdcTrigger_STATUS_MASK_PTR        ((reg8 *) AdcTrigger_PWMHW__SR0)
    #define AdcTrigger_CONTROL                (*(reg8 *) AdcTrigger_PWMHW__CFG0)
    #define AdcTrigger_CONTROL_PTR            ((reg8 *) AdcTrigger_PWMHW__CFG0)
    #define AdcTrigger_CONTROL2               (*(reg8 *) AdcTrigger_PWMHW__CFG1)
    #define AdcTrigger_CONTROL3               (*(reg8 *) AdcTrigger_PWMHW__CFG2)
    #define AdcTrigger_GLOBAL_ENABLE          (*(reg8 *) AdcTrigger_PWMHW__PM_ACT_CFG)
    #define AdcTrigger_GLOBAL_ENABLE_PTR      ( (reg8 *) AdcTrigger_PWMHW__PM_ACT_CFG)
    #define AdcTrigger_GLOBAL_STBY_ENABLE     (*(reg8 *) AdcTrigger_PWMHW__PM_STBY_CFG)
    #define AdcTrigger_GLOBAL_STBY_ENABLE_PTR ( (reg8 *) AdcTrigger_PWMHW__PM_STBY_CFG)


    /***********************************
    *          Constants
    ***********************************/

    /* Fixed Function Block Chosen */
    #define AdcTrigger_BLOCK_EN_MASK          (AdcTrigger_PWMHW__PM_ACT_MSK)
    #define AdcTrigger_BLOCK_STBY_EN_MASK     (AdcTrigger_PWMHW__PM_STBY_MSK)
    
    /* Control Register definitions */
    #define AdcTrigger_CTRL_ENABLE_SHIFT      (0x00u)

    /* As defined by Register map as MODE_CFG bits in CFG2*/
    #define AdcTrigger_CTRL_CMPMODE1_SHIFT    (0x04u)

    /* As defined by Register map */
    #define AdcTrigger_CTRL_DEAD_TIME_SHIFT   (0x06u)  

    /* Fixed Function Block Only CFG register bit definitions */
    /*  Set to compare mode */
    #define AdcTrigger_CFG0_MODE              (0x02u)   

    /* Enable the block to run */
    #define AdcTrigger_CFG0_ENABLE            (0x01u)   
    
    /* As defined by Register map as DB bit in CFG0 */
    #define AdcTrigger_CFG0_DB                (0x20u)   

    /* Control Register Bit Masks */
    #define AdcTrigger_CTRL_ENABLE            (uint8)((uint8)0x01u << AdcTrigger_CTRL_ENABLE_SHIFT)
    #define AdcTrigger_CTRL_RESET             (uint8)((uint8)0x01u << AdcTrigger_CTRL_RESET_SHIFT)
    #define AdcTrigger_CTRL_CMPMODE2_MASK     (uint8)((uint8)0x07u << AdcTrigger_CTRL_CMPMODE2_SHIFT)
    #define AdcTrigger_CTRL_CMPMODE1_MASK     (uint8)((uint8)0x07u << AdcTrigger_CTRL_CMPMODE1_SHIFT)

    /* Control2 Register Bit Masks */
    /* As defined in Register Map, Part of the TMRX_CFG1 register */
    #define AdcTrigger_CTRL2_IRQ_SEL_SHIFT    (0x00u)
    #define AdcTrigger_CTRL2_IRQ_SEL          (uint8)((uint8)0x01u << AdcTrigger_CTRL2_IRQ_SEL_SHIFT)

    /* Status Register Bit Locations */
    /* As defined by Register map as TC in SR0 */
    #define AdcTrigger_STATUS_TC_SHIFT        (0x07u)   
    
    /* As defined by the Register map as CAP_CMP in SR0 */
    #define AdcTrigger_STATUS_CMP1_SHIFT      (0x06u)   

    /* Status Register Interrupt Enable Bit Locations */
    #define AdcTrigger_STATUS_KILL_INT_EN_MASK_SHIFT          (0x00u)
    #define AdcTrigger_STATUS_TC_INT_EN_MASK_SHIFT            (AdcTrigger_STATUS_TC_SHIFT - 4u)
    #define AdcTrigger_STATUS_CMP2_INT_EN_MASK_SHIFT          (0x00u)
    #define AdcTrigger_STATUS_CMP1_INT_EN_MASK_SHIFT          (AdcTrigger_STATUS_CMP1_SHIFT - 4u)

    /* Status Register Bit Masks */
    #define AdcTrigger_STATUS_TC              (uint8)((uint8)0x01u << AdcTrigger_STATUS_TC_SHIFT)
    #define AdcTrigger_STATUS_CMP1            (uint8)((uint8)0x01u << AdcTrigger_STATUS_CMP1_SHIFT)

    /* Status Register Interrupt Bit Masks */
    #define AdcTrigger_STATUS_TC_INT_EN_MASK              (uint8)((uint8)AdcTrigger_STATUS_TC >> 4u)
    #define AdcTrigger_STATUS_CMP1_INT_EN_MASK            (uint8)((uint8)AdcTrigger_STATUS_CMP1 >> 4u)

    /*RT1 Synch Constants */
    #define AdcTrigger_RT1_SHIFT             (0x04u)

    /* Sync TC and CMP bit masks */
    #define AdcTrigger_RT1_MASK              (uint8)((uint8)0x03u << AdcTrigger_RT1_SHIFT)
    #define AdcTrigger_SYNC                  (uint8)((uint8)0x03u << AdcTrigger_RT1_SHIFT)
    #define AdcTrigger_SYNCDSI_SHIFT         (0x00u)

    /* Sync all DSI inputs */
    #define AdcTrigger_SYNCDSI_MASK          (uint8)((uint8)0x0Fu << AdcTrigger_SYNCDSI_SHIFT)

    /* Sync all DSI inputs */
    #define AdcTrigger_SYNCDSI_EN            (uint8)((uint8)0x0Fu << AdcTrigger_SYNCDSI_SHIFT)


#else
    #define AdcTrigger_STATUS                (*(reg8 *)   AdcTrigger_PWMUDB_genblk8_stsreg__STATUS_REG )
    #define AdcTrigger_STATUS_PTR            ((reg8 *)    AdcTrigger_PWMUDB_genblk8_stsreg__STATUS_REG )
    #define AdcTrigger_STATUS_MASK           (*(reg8 *)   AdcTrigger_PWMUDB_genblk8_stsreg__MASK_REG)
    #define AdcTrigger_STATUS_MASK_PTR       ((reg8 *)    AdcTrigger_PWMUDB_genblk8_stsreg__MASK_REG)
    #define AdcTrigger_STATUS_AUX_CTRL       (*(reg8 *)   AdcTrigger_PWMUDB_genblk8_stsreg__STATUS_AUX_CTL_REG)
    #define AdcTrigger_CONTROL               (*(reg8 *)   AdcTrigger_PWMUDB_genblk1_ctrlreg__CONTROL_REG)
    #define AdcTrigger_CONTROL_PTR           ((reg8 *)    AdcTrigger_PWMUDB_genblk1_ctrlreg__CONTROL_REG)


    /***********************************
    *          Constants
    ***********************************/

    /* Control Register bit definitions */
    #define AdcTrigger_CTRL_ENABLE_SHIFT      (0x07u)
    #define AdcTrigger_CTRL_RESET_SHIFT       (0x06u)
    #define AdcTrigger_CTRL_CMPMODE2_SHIFT    (0x03u)
    #define AdcTrigger_CTRL_CMPMODE1_SHIFT    (0x00u)
    #define AdcTrigger_CTRL_DEAD_TIME_SHIFT   (0x00u)   /* No Shift Needed for UDB block */
    
    /* Control Register Bit Masks */
    #define AdcTrigger_CTRL_ENABLE            (uint8)((uint8)0x01u << AdcTrigger_CTRL_ENABLE_SHIFT)
    #define AdcTrigger_CTRL_RESET             (uint8)((uint8)0x01u << AdcTrigger_CTRL_RESET_SHIFT)
    #define AdcTrigger_CTRL_CMPMODE2_MASK     (uint8)((uint8)0x07u << AdcTrigger_CTRL_CMPMODE2_SHIFT)
    #define AdcTrigger_CTRL_CMPMODE1_MASK     (uint8)((uint8)0x07u << AdcTrigger_CTRL_CMPMODE1_SHIFT)

    /* Status Register Bit Locations */
    #define AdcTrigger_STATUS_KILL_SHIFT          (0x05u)
    #define AdcTrigger_STATUS_FIFONEMPTY_SHIFT    (0x04u)
    #define AdcTrigger_STATUS_FIFOFULL_SHIFT      (0x03u)
    #define AdcTrigger_STATUS_TC_SHIFT            (0x02u)
    #define AdcTrigger_STATUS_CMP2_SHIFT          (0x01u)
    #define AdcTrigger_STATUS_CMP1_SHIFT          (0x00u)

    /* Status Register Interrupt Enable Bit Locations - UDB Status Interrupt Mask match Status Bit Locations*/
    #define AdcTrigger_STATUS_KILL_INT_EN_MASK_SHIFT          (AdcTrigger_STATUS_KILL_SHIFT)
    #define AdcTrigger_STATUS_FIFONEMPTY_INT_EN_MASK_SHIFT    (AdcTrigger_STATUS_FIFONEMPTY_SHIFT)
    #define AdcTrigger_STATUS_FIFOFULL_INT_EN_MASK_SHIFT      (AdcTrigger_STATUS_FIFOFULL_SHIFT)
    #define AdcTrigger_STATUS_TC_INT_EN_MASK_SHIFT            (AdcTrigger_STATUS_TC_SHIFT)
    #define AdcTrigger_STATUS_CMP2_INT_EN_MASK_SHIFT          (AdcTrigger_STATUS_CMP2_SHIFT)
    #define AdcTrigger_STATUS_CMP1_INT_EN_MASK_SHIFT          (AdcTrigger_STATUS_CMP1_SHIFT)

    /* Status Register Bit Masks */
    #define AdcTrigger_STATUS_KILL            (uint8)((uint8)0x00u << AdcTrigger_STATUS_KILL_SHIFT )
    #define AdcTrigger_STATUS_FIFOFULL        (uint8)((uint8)0x01u << AdcTrigger_STATUS_FIFOFULL_SHIFT)
    #define AdcTrigger_STATUS_FIFONEMPTY      (uint8)((uint8)0x01u << AdcTrigger_STATUS_FIFONEMPTY_SHIFT)
    #define AdcTrigger_STATUS_TC              (uint8)((uint8)0x01u << AdcTrigger_STATUS_TC_SHIFT)
    #define AdcTrigger_STATUS_CMP2            (uint8)((uint8)0x01u << AdcTrigger_STATUS_CMP2_SHIFT)
    #define AdcTrigger_STATUS_CMP1            (uint8)((uint8)0x01u << AdcTrigger_STATUS_CMP1_SHIFT)

    /* Status Register Interrupt Bit Masks  - UDB Status Interrupt Mask match Status Bit Locations */
    #define AdcTrigger_STATUS_KILL_INT_EN_MASK            (AdcTrigger_STATUS_KILL)
    #define AdcTrigger_STATUS_FIFOFULL_INT_EN_MASK        (AdcTrigger_STATUS_FIFOFULL)
    #define AdcTrigger_STATUS_FIFONEMPTY_INT_EN_MASK      (AdcTrigger_STATUS_FIFONEMPTY)
    #define AdcTrigger_STATUS_TC_INT_EN_MASK              (AdcTrigger_STATUS_TC)
    #define AdcTrigger_STATUS_CMP2_INT_EN_MASK            (AdcTrigger_STATUS_CMP2)
    #define AdcTrigger_STATUS_CMP1_INT_EN_MASK            (AdcTrigger_STATUS_CMP1)

    /* Datapath Auxillary Control Register bit definitions */
    #define AdcTrigger_AUX_CTRL_FIFO0_CLR         (0x01u)
    #define AdcTrigger_AUX_CTRL_FIFO1_CLR         (0x02u)
    #define AdcTrigger_AUX_CTRL_FIFO0_LVL         (0x04u)
    #define AdcTrigger_AUX_CTRL_FIFO1_LVL         (0x08u)
    #define AdcTrigger_STATUS_ACTL_INT_EN_MASK    (0x10u) /* As defined for the ACTL Register */
#endif /* AdcTrigger_UsingFixedFunction */

#endif  /* CY_PWM_AdcTrigger_H */


/* [] END OF FILE */
