/*******************************************************************************
* File Name: AdcSarSeq_PM.c
* Version 2.50
*
* Description:
*  This file provides Sleep/WakeUp APIs functionality.
*
* Note:
*
********************************************************************************
* Copyright 2008-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "AdcSarSeq.h"


/***************************************
* Local data allocation
***************************************/

static AdcSarSeq_BACKUP_STRUCT  AdcSarSeq_backup =
{
    AdcSarSeq_DISABLED,
    0u    
};


/*******************************************************************************
* Function Name: AdcSarSeq_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void AdcSarSeq_SaveConfig(void)
{
    /* All configuration registers are marked as [reset_all_retention] */
}


/*******************************************************************************
* Function Name: AdcSarSeq_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void AdcSarSeq_RestoreConfig(void)
{
    /* All configuration registers are marked as [reset_all_retention] */
}


/*******************************************************************************
* Function Name: AdcSarSeq_Sleep
********************************************************************************
*
* Summary:
*  Stops the ADC operation and saves the configuration registers and component
*  enable state. Should be called just prior to entering sleep.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  AdcSarSeq_backup - modified.
*
*******************************************************************************/
void AdcSarSeq_Sleep(void)
{
    /* During deepsleep/ hibernate mode keep SARMUX active, i.e. do not open
    *   all switches (disconnect), to be used for ADFT
    */
    AdcSarSeq_backup.dftRegVal = AdcSarSeq_SAR_DFT_CTRL_REG & (uint32)~AdcSarSeq_ADFT_OVERRIDE;
    AdcSarSeq_SAR_DFT_CTRL_REG |= AdcSarSeq_ADFT_OVERRIDE;
    if((AdcSarSeq_SAR_CTRL_REG  & AdcSarSeq_ENABLE) != 0u)
    {
        if((AdcSarSeq_SAR_SAMPLE_CTRL_REG & AdcSarSeq_CONTINUOUS_EN) != 0u)
        {
            AdcSarSeq_backup.enableState = AdcSarSeq_ENABLED | AdcSarSeq_STARTED;
        }
        else
        {
            AdcSarSeq_backup.enableState = AdcSarSeq_ENABLED;
        }
        AdcSarSeq_StopConvert();
        AdcSarSeq_Stop();
        
        /* Disable the SAR internal pump before entering the chip low power mode */
        if((AdcSarSeq_SAR_CTRL_REG & AdcSarSeq_BOOSTPUMP_EN) != 0u)
        {
            AdcSarSeq_SAR_CTRL_REG &= (uint32)~AdcSarSeq_BOOSTPUMP_EN;
            AdcSarSeq_backup.enableState |= AdcSarSeq_BOOSTPUMP_ENABLED;
        }
    }
    else
    {
        AdcSarSeq_backup.enableState = AdcSarSeq_DISABLED;
    }
}


/*******************************************************************************
* Function Name: AdcSarSeq_Wakeup
********************************************************************************
*
* Summary:
*  Restores the component enable state and configuration registers.
*  This should be called just after awaking from sleep mode.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  AdcSarSeq_backup - used.
*
*******************************************************************************/
void AdcSarSeq_Wakeup(void)
{
    AdcSarSeq_SAR_DFT_CTRL_REG = AdcSarSeq_backup.dftRegVal;
    if(AdcSarSeq_backup.enableState != AdcSarSeq_DISABLED)
    {
        /* Enable the SAR internal pump  */
        if((AdcSarSeq_backup.enableState & AdcSarSeq_BOOSTPUMP_ENABLED) != 0u)
        {
            AdcSarSeq_SAR_CTRL_REG |= AdcSarSeq_BOOSTPUMP_EN;
        }
        AdcSarSeq_Enable();
        if((AdcSarSeq_backup.enableState & AdcSarSeq_STARTED) != 0u)
        {
            AdcSarSeq_StartConvert();
        }
    }
}
/* [] END OF FILE */
