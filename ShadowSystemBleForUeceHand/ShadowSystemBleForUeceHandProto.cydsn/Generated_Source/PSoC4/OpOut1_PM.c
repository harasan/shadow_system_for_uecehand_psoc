/*******************************************************************************
* File Name: OpOut1.c  
* Version 2.20
*
* Description:
*  This file contains APIs to set up the Pins component for low power modes.
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "OpOut1.h"

static OpOut1_BACKUP_STRUCT  OpOut1_backup = {0u, 0u, 0u};


/*******************************************************************************
* Function Name: OpOut1_Sleep
****************************************************************************//**
*
* \brief Stores the pin configuration and prepares the pin for entering chip 
*  deep-sleep/hibernate modes. This function applies only to SIO and USBIO pins.
*  It should not be called for GPIO or GPIO_OVT pins.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None 
*  
* \sideeffect
*  For SIO pins, this function configures the pin input threshold to CMOS and
*  drive level to Vddio. This is needed for SIO pins when in device 
*  deep-sleep/hibernate modes.
*
* \funcusage
*  \snippet OpOut1_SUT.c usage_OpOut1_Sleep_Wakeup
*******************************************************************************/
void OpOut1_Sleep(void)
{
    #if defined(OpOut1__PC)
        OpOut1_backup.pcState = OpOut1_PC;
    #else
        #if (CY_PSOC4_4200L)
            /* Save the regulator state and put the PHY into suspend mode */
            OpOut1_backup.usbState = OpOut1_CR1_REG;
            OpOut1_USB_POWER_REG |= OpOut1_USBIO_ENTER_SLEEP;
            OpOut1_CR1_REG &= OpOut1_USBIO_CR1_OFF;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(OpOut1__SIO)
        OpOut1_backup.sioState = OpOut1_SIO_REG;
        /* SIO requires unregulated output buffer and single ended input buffer */
        OpOut1_SIO_REG &= (uint32)(~OpOut1_SIO_LPM_MASK);
    #endif  
}


/*******************************************************************************
* Function Name: OpOut1_Wakeup
****************************************************************************//**
*
* \brief Restores the pin configuration that was saved during Pin_Sleep(). This 
* function applies only to SIO and USBIO pins. It should not be called for
* GPIO or GPIO_OVT pins.
*
* For USBIO pins, the wakeup is only triggered for falling edge interrupts.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None
*  
* \funcusage
*  Refer to OpOut1_Sleep() for an example usage.
*******************************************************************************/
void OpOut1_Wakeup(void)
{
    #if defined(OpOut1__PC)
        OpOut1_PC = OpOut1_backup.pcState;
    #else
        #if (CY_PSOC4_4200L)
            /* Restore the regulator state and come out of suspend mode */
            OpOut1_USB_POWER_REG &= OpOut1_USBIO_EXIT_SLEEP_PH1;
            OpOut1_CR1_REG = OpOut1_backup.usbState;
            OpOut1_USB_POWER_REG &= OpOut1_USBIO_EXIT_SLEEP_PH2;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(OpOut1__SIO)
        OpOut1_SIO_REG = OpOut1_backup.sioState;
    #endif
}


/* [] END OF FILE */
