/*******************************************************************************
* File Name: AdcTrigerClock.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_AdcTrigerClock_H)
#define CY_CLOCK_AdcTrigerClock_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
*        Function Prototypes
***************************************/
#if defined CYREG_PERI_DIV_CMD

void AdcTrigerClock_StartEx(uint32 alignClkDiv);
#define AdcTrigerClock_Start() \
    AdcTrigerClock_StartEx(AdcTrigerClock__PA_DIV_ID)

#else

void AdcTrigerClock_Start(void);

#endif/* CYREG_PERI_DIV_CMD */

void AdcTrigerClock_Stop(void);

void AdcTrigerClock_SetFractionalDividerRegister(uint16 clkDivider, uint8 clkFractional);

uint16 AdcTrigerClock_GetDividerRegister(void);
uint8  AdcTrigerClock_GetFractionalDividerRegister(void);

#define AdcTrigerClock_Enable()                         AdcTrigerClock_Start()
#define AdcTrigerClock_Disable()                        AdcTrigerClock_Stop()
#define AdcTrigerClock_SetDividerRegister(clkDivider, reset)  \
    AdcTrigerClock_SetFractionalDividerRegister((clkDivider), 0u)
#define AdcTrigerClock_SetDivider(clkDivider)           AdcTrigerClock_SetDividerRegister((clkDivider), 1u)
#define AdcTrigerClock_SetDividerValue(clkDivider)      AdcTrigerClock_SetDividerRegister((clkDivider) - 1u, 1u)


/***************************************
*             Registers
***************************************/
#if defined CYREG_PERI_DIV_CMD

#define AdcTrigerClock_DIV_ID     AdcTrigerClock__DIV_ID

#define AdcTrigerClock_CMD_REG    (*(reg32 *)CYREG_PERI_DIV_CMD)
#define AdcTrigerClock_CTRL_REG   (*(reg32 *)AdcTrigerClock__CTRL_REGISTER)
#define AdcTrigerClock_DIV_REG    (*(reg32 *)AdcTrigerClock__DIV_REGISTER)

#define AdcTrigerClock_CMD_DIV_SHIFT          (0u)
#define AdcTrigerClock_CMD_PA_DIV_SHIFT       (8u)
#define AdcTrigerClock_CMD_DISABLE_SHIFT      (30u)
#define AdcTrigerClock_CMD_ENABLE_SHIFT       (31u)

#define AdcTrigerClock_CMD_DISABLE_MASK       ((uint32)((uint32)1u << AdcTrigerClock_CMD_DISABLE_SHIFT))
#define AdcTrigerClock_CMD_ENABLE_MASK        ((uint32)((uint32)1u << AdcTrigerClock_CMD_ENABLE_SHIFT))

#define AdcTrigerClock_DIV_FRAC_MASK  (0x000000F8u)
#define AdcTrigerClock_DIV_FRAC_SHIFT (3u)
#define AdcTrigerClock_DIV_INT_MASK   (0xFFFFFF00u)
#define AdcTrigerClock_DIV_INT_SHIFT  (8u)

#else 

#define AdcTrigerClock_DIV_REG        (*(reg32 *)AdcTrigerClock__REGISTER)
#define AdcTrigerClock_ENABLE_REG     AdcTrigerClock_DIV_REG
#define AdcTrigerClock_DIV_FRAC_MASK  AdcTrigerClock__FRAC_MASK
#define AdcTrigerClock_DIV_FRAC_SHIFT (16u)
#define AdcTrigerClock_DIV_INT_MASK   AdcTrigerClock__DIVIDER_MASK
#define AdcTrigerClock_DIV_INT_SHIFT  (0u)

#endif/* CYREG_PERI_DIV_CMD */

#endif /* !defined(CY_CLOCK_AdcTrigerClock_H) */

/* [] END OF FILE */
