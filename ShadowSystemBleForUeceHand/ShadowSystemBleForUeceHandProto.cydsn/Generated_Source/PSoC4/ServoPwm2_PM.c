/*******************************************************************************
* File Name: ServoPwm2_PM.c
* Version 2.10
*
* Description:
*  This file contains the setup, control, and status commands to support
*  the component operations in the low power mode.
*
* Note:
*  None
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "ServoPwm2.h"

static ServoPwm2_BACKUP_STRUCT ServoPwm2_backup;


/*******************************************************************************
* Function Name: ServoPwm2_SaveConfig
********************************************************************************
*
* Summary:
*  All configuration registers are retention. Nothing to save here.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm2_SaveConfig(void)
{

}


/*******************************************************************************
* Function Name: ServoPwm2_Sleep
********************************************************************************
*
* Summary:
*  Stops the component operation and saves the user configuration.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm2_Sleep(void)
{
    if(0u != (ServoPwm2_BLOCK_CONTROL_REG & ServoPwm2_MASK))
    {
        ServoPwm2_backup.enableState = 1u;
    }
    else
    {
        ServoPwm2_backup.enableState = 0u;
    }

    ServoPwm2_Stop();
    ServoPwm2_SaveConfig();
}


/*******************************************************************************
* Function Name: ServoPwm2_RestoreConfig
********************************************************************************
*
* Summary:
*  All configuration registers are retention. Nothing to restore here.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm2_RestoreConfig(void)
{

}


/*******************************************************************************
* Function Name: ServoPwm2_Wakeup
********************************************************************************
*
* Summary:
*  Restores the user configuration and restores the enable state.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm2_Wakeup(void)
{
    ServoPwm2_RestoreConfig();

    if(0u != ServoPwm2_backup.enableState)
    {
        ServoPwm2_Enable();
    }
}


/* [] END OF FILE */
