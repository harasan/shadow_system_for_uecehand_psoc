/*******************************************************************************
* File Name: Adc_PM.c
* Version 2.50
*
* Description:
*  This file provides Sleep/WakeUp APIs functionality.
*
* Note:
*
********************************************************************************
* Copyright 2008-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "Adc.h"


/***************************************
* Local data allocation
***************************************/

static Adc_BACKUP_STRUCT  Adc_backup =
{
    Adc_DISABLED,
    0u    
};


/*******************************************************************************
* Function Name: Adc_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void Adc_SaveConfig(void)
{
    /* All configuration registers are marked as [reset_all_retention] */
}


/*******************************************************************************
* Function Name: Adc_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void Adc_RestoreConfig(void)
{
    /* All configuration registers are marked as [reset_all_retention] */
}


/*******************************************************************************
* Function Name: Adc_Sleep
********************************************************************************
*
* Summary:
*  Stops the ADC operation and saves the configuration registers and component
*  enable state. Should be called just prior to entering sleep.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  Adc_backup - modified.
*
*******************************************************************************/
void Adc_Sleep(void)
{
    /* During deepsleep/ hibernate mode keep SARMUX active, i.e. do not open
    *   all switches (disconnect), to be used for ADFT
    */
    Adc_backup.dftRegVal = Adc_SAR_DFT_CTRL_REG & (uint32)~Adc_ADFT_OVERRIDE;
    Adc_SAR_DFT_CTRL_REG |= Adc_ADFT_OVERRIDE;
    if((Adc_SAR_CTRL_REG  & Adc_ENABLE) != 0u)
    {
        if((Adc_SAR_SAMPLE_CTRL_REG & Adc_CONTINUOUS_EN) != 0u)
        {
            Adc_backup.enableState = Adc_ENABLED | Adc_STARTED;
        }
        else
        {
            Adc_backup.enableState = Adc_ENABLED;
        }
        Adc_StopConvert();
        Adc_Stop();
        
        /* Disable the SAR internal pump before entering the chip low power mode */
        if((Adc_SAR_CTRL_REG & Adc_BOOSTPUMP_EN) != 0u)
        {
            Adc_SAR_CTRL_REG &= (uint32)~Adc_BOOSTPUMP_EN;
            Adc_backup.enableState |= Adc_BOOSTPUMP_ENABLED;
        }
    }
    else
    {
        Adc_backup.enableState = Adc_DISABLED;
    }
}


/*******************************************************************************
* Function Name: Adc_Wakeup
********************************************************************************
*
* Summary:
*  Restores the component enable state and configuration registers.
*  This should be called just after awaking from sleep mode.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  Adc_backup - used.
*
*******************************************************************************/
void Adc_Wakeup(void)
{
    Adc_SAR_DFT_CTRL_REG = Adc_backup.dftRegVal;
    if(Adc_backup.enableState != Adc_DISABLED)
    {
        /* Enable the SAR internal pump  */
        if((Adc_backup.enableState & Adc_BOOSTPUMP_ENABLED) != 0u)
        {
            Adc_SAR_CTRL_REG |= Adc_BOOSTPUMP_EN;
        }
        Adc_Enable();
        if((Adc_backup.enableState & Adc_STARTED) != 0u)
        {
            Adc_StartConvert();
        }
    }
}
/* [] END OF FILE */
