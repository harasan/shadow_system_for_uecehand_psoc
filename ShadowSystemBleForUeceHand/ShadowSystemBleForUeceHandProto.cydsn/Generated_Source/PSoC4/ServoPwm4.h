/*******************************************************************************
* File Name: ServoPwm4.h
* Version 2.10
*
* Description:
*  This file provides constants and parameter values for the ServoPwm4
*  component.
*
* Note:
*  None
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_TCPWM_ServoPwm4_H)
#define CY_TCPWM_ServoPwm4_H


#include "CyLib.h"
#include "cytypes.h"
#include "cyfitter.h"


/*******************************************************************************
* Internal Type defines
*******************************************************************************/

/* Structure to save state before go to sleep */
typedef struct
{
    uint8  enableState;
} ServoPwm4_BACKUP_STRUCT;


/*******************************************************************************
* Variables
*******************************************************************************/
extern uint8  ServoPwm4_initVar;


/***************************************
*   Conditional Compilation Parameters
****************************************/

#define ServoPwm4_CY_TCPWM_V2                    (CYIPBLOCK_m0s8tcpwm_VERSION == 2u)
#define ServoPwm4_CY_TCPWM_4000                  (CY_PSOC4_4000)

/* TCPWM Configuration */
#define ServoPwm4_CONFIG                         (7lu)

/* Quad Mode */
/* Parameters */
#define ServoPwm4_QUAD_ENCODING_MODES            (0lu)
#define ServoPwm4_QUAD_AUTO_START                (1lu)

/* Signal modes */
#define ServoPwm4_QUAD_INDEX_SIGNAL_MODE         (0lu)
#define ServoPwm4_QUAD_PHIA_SIGNAL_MODE          (3lu)
#define ServoPwm4_QUAD_PHIB_SIGNAL_MODE          (3lu)
#define ServoPwm4_QUAD_STOP_SIGNAL_MODE          (0lu)

/* Signal present */
#define ServoPwm4_QUAD_INDEX_SIGNAL_PRESENT      (0lu)
#define ServoPwm4_QUAD_STOP_SIGNAL_PRESENT       (0lu)

/* Interrupt Mask */
#define ServoPwm4_QUAD_INTERRUPT_MASK            (1lu)

/* Timer/Counter Mode */
/* Parameters */
#define ServoPwm4_TC_RUN_MODE                    (0lu)
#define ServoPwm4_TC_COUNTER_MODE                (0lu)
#define ServoPwm4_TC_COMP_CAP_MODE               (2lu)
#define ServoPwm4_TC_PRESCALER                   (0lu)

/* Signal modes */
#define ServoPwm4_TC_RELOAD_SIGNAL_MODE          (0lu)
#define ServoPwm4_TC_COUNT_SIGNAL_MODE           (3lu)
#define ServoPwm4_TC_START_SIGNAL_MODE           (0lu)
#define ServoPwm4_TC_STOP_SIGNAL_MODE            (0lu)
#define ServoPwm4_TC_CAPTURE_SIGNAL_MODE         (0lu)

/* Signal present */
#define ServoPwm4_TC_RELOAD_SIGNAL_PRESENT       (0lu)
#define ServoPwm4_TC_COUNT_SIGNAL_PRESENT        (0lu)
#define ServoPwm4_TC_START_SIGNAL_PRESENT        (0lu)
#define ServoPwm4_TC_STOP_SIGNAL_PRESENT         (0lu)
#define ServoPwm4_TC_CAPTURE_SIGNAL_PRESENT      (0lu)

/* Interrupt Mask */
#define ServoPwm4_TC_INTERRUPT_MASK              (1lu)

/* PWM Mode */
/* Parameters */
#define ServoPwm4_PWM_KILL_EVENT                 (0lu)
#define ServoPwm4_PWM_STOP_EVENT                 (0lu)
#define ServoPwm4_PWM_MODE                       (4lu)
#define ServoPwm4_PWM_OUT_N_INVERT               (0lu)
#define ServoPwm4_PWM_OUT_INVERT                 (1lu)
#define ServoPwm4_PWM_ALIGN                      (0lu)
#define ServoPwm4_PWM_RUN_MODE                   (0lu)
#define ServoPwm4_PWM_DEAD_TIME_CYCLE            (0lu)
#define ServoPwm4_PWM_PRESCALER                  (0lu)

/* Signal modes */
#define ServoPwm4_PWM_RELOAD_SIGNAL_MODE         (0lu)
#define ServoPwm4_PWM_COUNT_SIGNAL_MODE          (3lu)
#define ServoPwm4_PWM_START_SIGNAL_MODE          (0lu)
#define ServoPwm4_PWM_STOP_SIGNAL_MODE           (0lu)
#define ServoPwm4_PWM_SWITCH_SIGNAL_MODE         (0lu)

/* Signal present */
#define ServoPwm4_PWM_RELOAD_SIGNAL_PRESENT      (0lu)
#define ServoPwm4_PWM_COUNT_SIGNAL_PRESENT       (0lu)
#define ServoPwm4_PWM_START_SIGNAL_PRESENT       (0lu)
#define ServoPwm4_PWM_STOP_SIGNAL_PRESENT        (0lu)
#define ServoPwm4_PWM_SWITCH_SIGNAL_PRESENT      (0lu)

/* Interrupt Mask */
#define ServoPwm4_PWM_INTERRUPT_MASK             (1lu)


/***************************************
*    Initial Parameter Constants
***************************************/

/* Timer/Counter Mode */
#define ServoPwm4_TC_PERIOD_VALUE                (65535lu)
#define ServoPwm4_TC_COMPARE_VALUE               (65535lu)
#define ServoPwm4_TC_COMPARE_BUF_VALUE           (65535lu)
#define ServoPwm4_TC_COMPARE_SWAP                (0lu)

/* PWM Mode */
#define ServoPwm4_PWM_PERIOD_VALUE               (59999lu)
#define ServoPwm4_PWM_PERIOD_BUF_VALUE           (65535lu)
#define ServoPwm4_PWM_PERIOD_SWAP                (0lu)
#define ServoPwm4_PWM_COMPARE_VALUE              (0lu)
#define ServoPwm4_PWM_COMPARE_BUF_VALUE          (65535lu)
#define ServoPwm4_PWM_COMPARE_SWAP               (0lu)


/***************************************
*    Enumerated Types and Parameters
***************************************/

#define ServoPwm4__LEFT 0
#define ServoPwm4__RIGHT 1
#define ServoPwm4__CENTER 2
#define ServoPwm4__ASYMMETRIC 3

#define ServoPwm4__X1 0
#define ServoPwm4__X2 1
#define ServoPwm4__X4 2

#define ServoPwm4__PWM 4
#define ServoPwm4__PWM_DT 5
#define ServoPwm4__PWM_PR 6

#define ServoPwm4__INVERSE 1
#define ServoPwm4__DIRECT 0

#define ServoPwm4__CAPTURE 2
#define ServoPwm4__COMPARE 0

#define ServoPwm4__TRIG_LEVEL 3
#define ServoPwm4__TRIG_RISING 0
#define ServoPwm4__TRIG_FALLING 1
#define ServoPwm4__TRIG_BOTH 2

#define ServoPwm4__INTR_MASK_TC 1
#define ServoPwm4__INTR_MASK_CC_MATCH 2
#define ServoPwm4__INTR_MASK_NONE 0
#define ServoPwm4__INTR_MASK_TC_CC 3

#define ServoPwm4__UNCONFIG 8
#define ServoPwm4__TIMER 1
#define ServoPwm4__QUAD 3
#define ServoPwm4__PWM_SEL 7

#define ServoPwm4__COUNT_UP 0
#define ServoPwm4__COUNT_DOWN 1
#define ServoPwm4__COUNT_UPDOWN0 2
#define ServoPwm4__COUNT_UPDOWN1 3


/* Prescaler */
#define ServoPwm4_PRESCALE_DIVBY1                ((uint32)(0u << ServoPwm4_PRESCALER_SHIFT))
#define ServoPwm4_PRESCALE_DIVBY2                ((uint32)(1u << ServoPwm4_PRESCALER_SHIFT))
#define ServoPwm4_PRESCALE_DIVBY4                ((uint32)(2u << ServoPwm4_PRESCALER_SHIFT))
#define ServoPwm4_PRESCALE_DIVBY8                ((uint32)(3u << ServoPwm4_PRESCALER_SHIFT))
#define ServoPwm4_PRESCALE_DIVBY16               ((uint32)(4u << ServoPwm4_PRESCALER_SHIFT))
#define ServoPwm4_PRESCALE_DIVBY32               ((uint32)(5u << ServoPwm4_PRESCALER_SHIFT))
#define ServoPwm4_PRESCALE_DIVBY64               ((uint32)(6u << ServoPwm4_PRESCALER_SHIFT))
#define ServoPwm4_PRESCALE_DIVBY128              ((uint32)(7u << ServoPwm4_PRESCALER_SHIFT))

/* TCPWM set modes */
#define ServoPwm4_MODE_TIMER_COMPARE             ((uint32)(ServoPwm4__COMPARE         <<  \
                                                                  ServoPwm4_MODE_SHIFT))
#define ServoPwm4_MODE_TIMER_CAPTURE             ((uint32)(ServoPwm4__CAPTURE         <<  \
                                                                  ServoPwm4_MODE_SHIFT))
#define ServoPwm4_MODE_QUAD                      ((uint32)(ServoPwm4__QUAD            <<  \
                                                                  ServoPwm4_MODE_SHIFT))
#define ServoPwm4_MODE_PWM                       ((uint32)(ServoPwm4__PWM             <<  \
                                                                  ServoPwm4_MODE_SHIFT))
#define ServoPwm4_MODE_PWM_DT                    ((uint32)(ServoPwm4__PWM_DT          <<  \
                                                                  ServoPwm4_MODE_SHIFT))
#define ServoPwm4_MODE_PWM_PR                    ((uint32)(ServoPwm4__PWM_PR          <<  \
                                                                  ServoPwm4_MODE_SHIFT))

/* Quad Modes */
#define ServoPwm4_MODE_X1                        ((uint32)(ServoPwm4__X1              <<  \
                                                                  ServoPwm4_QUAD_MODE_SHIFT))
#define ServoPwm4_MODE_X2                        ((uint32)(ServoPwm4__X2              <<  \
                                                                  ServoPwm4_QUAD_MODE_SHIFT))
#define ServoPwm4_MODE_X4                        ((uint32)(ServoPwm4__X4              <<  \
                                                                  ServoPwm4_QUAD_MODE_SHIFT))

/* Counter modes */
#define ServoPwm4_COUNT_UP                       ((uint32)(ServoPwm4__COUNT_UP        <<  \
                                                                  ServoPwm4_UPDOWN_SHIFT))
#define ServoPwm4_COUNT_DOWN                     ((uint32)(ServoPwm4__COUNT_DOWN      <<  \
                                                                  ServoPwm4_UPDOWN_SHIFT))
#define ServoPwm4_COUNT_UPDOWN0                  ((uint32)(ServoPwm4__COUNT_UPDOWN0   <<  \
                                                                  ServoPwm4_UPDOWN_SHIFT))
#define ServoPwm4_COUNT_UPDOWN1                  ((uint32)(ServoPwm4__COUNT_UPDOWN1   <<  \
                                                                  ServoPwm4_UPDOWN_SHIFT))

/* PWM output invert */
#define ServoPwm4_INVERT_LINE                    ((uint32)(ServoPwm4__INVERSE         <<  \
                                                                  ServoPwm4_INV_OUT_SHIFT))
#define ServoPwm4_INVERT_LINE_N                  ((uint32)(ServoPwm4__INVERSE         <<  \
                                                                  ServoPwm4_INV_COMPL_OUT_SHIFT))

/* Trigger modes */
#define ServoPwm4_TRIG_RISING                    ((uint32)ServoPwm4__TRIG_RISING)
#define ServoPwm4_TRIG_FALLING                   ((uint32)ServoPwm4__TRIG_FALLING)
#define ServoPwm4_TRIG_BOTH                      ((uint32)ServoPwm4__TRIG_BOTH)
#define ServoPwm4_TRIG_LEVEL                     ((uint32)ServoPwm4__TRIG_LEVEL)

/* Interrupt mask */
#define ServoPwm4_INTR_MASK_TC                   ((uint32)ServoPwm4__INTR_MASK_TC)
#define ServoPwm4_INTR_MASK_CC_MATCH             ((uint32)ServoPwm4__INTR_MASK_CC_MATCH)

/* PWM Output Controls */
#define ServoPwm4_CC_MATCH_SET                   (0x00u)
#define ServoPwm4_CC_MATCH_CLEAR                 (0x01u)
#define ServoPwm4_CC_MATCH_INVERT                (0x02u)
#define ServoPwm4_CC_MATCH_NO_CHANGE             (0x03u)
#define ServoPwm4_OVERLOW_SET                    (0x00u)
#define ServoPwm4_OVERLOW_CLEAR                  (0x04u)
#define ServoPwm4_OVERLOW_INVERT                 (0x08u)
#define ServoPwm4_OVERLOW_NO_CHANGE              (0x0Cu)
#define ServoPwm4_UNDERFLOW_SET                  (0x00u)
#define ServoPwm4_UNDERFLOW_CLEAR                (0x10u)
#define ServoPwm4_UNDERFLOW_INVERT               (0x20u)
#define ServoPwm4_UNDERFLOW_NO_CHANGE            (0x30u)

/* PWM Align */
#define ServoPwm4_PWM_MODE_LEFT                  (ServoPwm4_CC_MATCH_CLEAR        |   \
                                                         ServoPwm4_OVERLOW_SET           |   \
                                                         ServoPwm4_UNDERFLOW_NO_CHANGE)
#define ServoPwm4_PWM_MODE_RIGHT                 (ServoPwm4_CC_MATCH_SET          |   \
                                                         ServoPwm4_OVERLOW_NO_CHANGE     |   \
                                                         ServoPwm4_UNDERFLOW_CLEAR)
#define ServoPwm4_PWM_MODE_ASYM                  (ServoPwm4_CC_MATCH_INVERT       |   \
                                                         ServoPwm4_OVERLOW_SET           |   \
                                                         ServoPwm4_UNDERFLOW_CLEAR)

#if (ServoPwm4_CY_TCPWM_V2)
    #if(ServoPwm4_CY_TCPWM_4000)
        #define ServoPwm4_PWM_MODE_CENTER                (ServoPwm4_CC_MATCH_INVERT       |   \
                                                                 ServoPwm4_OVERLOW_NO_CHANGE     |   \
                                                                 ServoPwm4_UNDERFLOW_CLEAR)
    #else
        #define ServoPwm4_PWM_MODE_CENTER                (ServoPwm4_CC_MATCH_INVERT       |   \
                                                                 ServoPwm4_OVERLOW_SET           |   \
                                                                 ServoPwm4_UNDERFLOW_CLEAR)
    #endif /* (ServoPwm4_CY_TCPWM_4000) */
#else
    #define ServoPwm4_PWM_MODE_CENTER                (ServoPwm4_CC_MATCH_INVERT       |   \
                                                             ServoPwm4_OVERLOW_NO_CHANGE     |   \
                                                             ServoPwm4_UNDERFLOW_CLEAR)
#endif /* (ServoPwm4_CY_TCPWM_NEW) */

/* Command operations without condition */
#define ServoPwm4_CMD_CAPTURE                    (0u)
#define ServoPwm4_CMD_RELOAD                     (8u)
#define ServoPwm4_CMD_STOP                       (16u)
#define ServoPwm4_CMD_START                      (24u)

/* Status */
#define ServoPwm4_STATUS_DOWN                    (1u)
#define ServoPwm4_STATUS_RUNNING                 (2u)


/***************************************
*        Function Prototypes
****************************************/

void   ServoPwm4_Init(void);
void   ServoPwm4_Enable(void);
void   ServoPwm4_Start(void);
void   ServoPwm4_Stop(void);

void   ServoPwm4_SetMode(uint32 mode);
void   ServoPwm4_SetCounterMode(uint32 counterMode);
void   ServoPwm4_SetPWMMode(uint32 modeMask);
void   ServoPwm4_SetQDMode(uint32 qdMode);

void   ServoPwm4_SetPrescaler(uint32 prescaler);
void   ServoPwm4_TriggerCommand(uint32 mask, uint32 command);
void   ServoPwm4_SetOneShot(uint32 oneShotEnable);
uint32 ServoPwm4_ReadStatus(void);

void   ServoPwm4_SetPWMSyncKill(uint32 syncKillEnable);
void   ServoPwm4_SetPWMStopOnKill(uint32 stopOnKillEnable);
void   ServoPwm4_SetPWMDeadTime(uint32 deadTime);
void   ServoPwm4_SetPWMInvert(uint32 mask);

void   ServoPwm4_SetInterruptMode(uint32 interruptMask);
uint32 ServoPwm4_GetInterruptSourceMasked(void);
uint32 ServoPwm4_GetInterruptSource(void);
void   ServoPwm4_ClearInterrupt(uint32 interruptMask);
void   ServoPwm4_SetInterrupt(uint32 interruptMask);

void   ServoPwm4_WriteCounter(uint32 count);
uint32 ServoPwm4_ReadCounter(void);

uint32 ServoPwm4_ReadCapture(void);
uint32 ServoPwm4_ReadCaptureBuf(void);

void   ServoPwm4_WritePeriod(uint32 period);
uint32 ServoPwm4_ReadPeriod(void);
void   ServoPwm4_WritePeriodBuf(uint32 periodBuf);
uint32 ServoPwm4_ReadPeriodBuf(void);

void   ServoPwm4_WriteCompare(uint32 compare);
uint32 ServoPwm4_ReadCompare(void);
void   ServoPwm4_WriteCompareBuf(uint32 compareBuf);
uint32 ServoPwm4_ReadCompareBuf(void);

void   ServoPwm4_SetPeriodSwap(uint32 swapEnable);
void   ServoPwm4_SetCompareSwap(uint32 swapEnable);

void   ServoPwm4_SetCaptureMode(uint32 triggerMode);
void   ServoPwm4_SetReloadMode(uint32 triggerMode);
void   ServoPwm4_SetStartMode(uint32 triggerMode);
void   ServoPwm4_SetStopMode(uint32 triggerMode);
void   ServoPwm4_SetCountMode(uint32 triggerMode);

void   ServoPwm4_SaveConfig(void);
void   ServoPwm4_RestoreConfig(void);
void   ServoPwm4_Sleep(void);
void   ServoPwm4_Wakeup(void);


/***************************************
*             Registers
***************************************/

#define ServoPwm4_BLOCK_CONTROL_REG              (*(reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__TCPWM_CTRL )
#define ServoPwm4_BLOCK_CONTROL_PTR              ( (reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__TCPWM_CTRL )
#define ServoPwm4_COMMAND_REG                    (*(reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__TCPWM_CMD )
#define ServoPwm4_COMMAND_PTR                    ( (reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__TCPWM_CMD )
#define ServoPwm4_INTRRUPT_CAUSE_REG             (*(reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__TCPWM_INTR_CAUSE )
#define ServoPwm4_INTRRUPT_CAUSE_PTR             ( (reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__TCPWM_INTR_CAUSE )
#define ServoPwm4_CONTROL_REG                    (*(reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__CTRL )
#define ServoPwm4_CONTROL_PTR                    ( (reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__CTRL )
#define ServoPwm4_STATUS_REG                     (*(reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__STATUS )
#define ServoPwm4_STATUS_PTR                     ( (reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__STATUS )
#define ServoPwm4_COUNTER_REG                    (*(reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__COUNTER )
#define ServoPwm4_COUNTER_PTR                    ( (reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__COUNTER )
#define ServoPwm4_COMP_CAP_REG                   (*(reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__CC )
#define ServoPwm4_COMP_CAP_PTR                   ( (reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__CC )
#define ServoPwm4_COMP_CAP_BUF_REG               (*(reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__CC_BUFF )
#define ServoPwm4_COMP_CAP_BUF_PTR               ( (reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__CC_BUFF )
#define ServoPwm4_PERIOD_REG                     (*(reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__PERIOD )
#define ServoPwm4_PERIOD_PTR                     ( (reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__PERIOD )
#define ServoPwm4_PERIOD_BUF_REG                 (*(reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__PERIOD_BUFF )
#define ServoPwm4_PERIOD_BUF_PTR                 ( (reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__PERIOD_BUFF )
#define ServoPwm4_TRIG_CONTROL0_REG              (*(reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__TR_CTRL0 )
#define ServoPwm4_TRIG_CONTROL0_PTR              ( (reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__TR_CTRL0 )
#define ServoPwm4_TRIG_CONTROL1_REG              (*(reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__TR_CTRL1 )
#define ServoPwm4_TRIG_CONTROL1_PTR              ( (reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__TR_CTRL1 )
#define ServoPwm4_TRIG_CONTROL2_REG              (*(reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__TR_CTRL2 )
#define ServoPwm4_TRIG_CONTROL2_PTR              ( (reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__TR_CTRL2 )
#define ServoPwm4_INTERRUPT_REQ_REG              (*(reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__INTR )
#define ServoPwm4_INTERRUPT_REQ_PTR              ( (reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__INTR )
#define ServoPwm4_INTERRUPT_SET_REG              (*(reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__INTR_SET )
#define ServoPwm4_INTERRUPT_SET_PTR              ( (reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__INTR_SET )
#define ServoPwm4_INTERRUPT_MASK_REG             (*(reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__INTR_MASK )
#define ServoPwm4_INTERRUPT_MASK_PTR             ( (reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__INTR_MASK )
#define ServoPwm4_INTERRUPT_MASKED_REG           (*(reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__INTR_MASKED )
#define ServoPwm4_INTERRUPT_MASKED_PTR           ( (reg32 *) ServoPwm4_cy_m0s8_tcpwm_1__INTR_MASKED )


/***************************************
*       Registers Constants
***************************************/

/* Mask */
#define ServoPwm4_MASK                           ((uint32)ServoPwm4_cy_m0s8_tcpwm_1__TCPWM_CTRL_MASK)

/* Shift constants for control register */
#define ServoPwm4_RELOAD_CC_SHIFT                (0u)
#define ServoPwm4_RELOAD_PERIOD_SHIFT            (1u)
#define ServoPwm4_PWM_SYNC_KILL_SHIFT            (2u)
#define ServoPwm4_PWM_STOP_KILL_SHIFT            (3u)
#define ServoPwm4_PRESCALER_SHIFT                (8u)
#define ServoPwm4_UPDOWN_SHIFT                   (16u)
#define ServoPwm4_ONESHOT_SHIFT                  (18u)
#define ServoPwm4_QUAD_MODE_SHIFT                (20u)
#define ServoPwm4_INV_OUT_SHIFT                  (20u)
#define ServoPwm4_INV_COMPL_OUT_SHIFT            (21u)
#define ServoPwm4_MODE_SHIFT                     (24u)

/* Mask constants for control register */
#define ServoPwm4_RELOAD_CC_MASK                 ((uint32)(ServoPwm4_1BIT_MASK        <<  \
                                                                            ServoPwm4_RELOAD_CC_SHIFT))
#define ServoPwm4_RELOAD_PERIOD_MASK             ((uint32)(ServoPwm4_1BIT_MASK        <<  \
                                                                            ServoPwm4_RELOAD_PERIOD_SHIFT))
#define ServoPwm4_PWM_SYNC_KILL_MASK             ((uint32)(ServoPwm4_1BIT_MASK        <<  \
                                                                            ServoPwm4_PWM_SYNC_KILL_SHIFT))
#define ServoPwm4_PWM_STOP_KILL_MASK             ((uint32)(ServoPwm4_1BIT_MASK        <<  \
                                                                            ServoPwm4_PWM_STOP_KILL_SHIFT))
#define ServoPwm4_PRESCALER_MASK                 ((uint32)(ServoPwm4_8BIT_MASK        <<  \
                                                                            ServoPwm4_PRESCALER_SHIFT))
#define ServoPwm4_UPDOWN_MASK                    ((uint32)(ServoPwm4_2BIT_MASK        <<  \
                                                                            ServoPwm4_UPDOWN_SHIFT))
#define ServoPwm4_ONESHOT_MASK                   ((uint32)(ServoPwm4_1BIT_MASK        <<  \
                                                                            ServoPwm4_ONESHOT_SHIFT))
#define ServoPwm4_QUAD_MODE_MASK                 ((uint32)(ServoPwm4_3BIT_MASK        <<  \
                                                                            ServoPwm4_QUAD_MODE_SHIFT))
#define ServoPwm4_INV_OUT_MASK                   ((uint32)(ServoPwm4_2BIT_MASK        <<  \
                                                                            ServoPwm4_INV_OUT_SHIFT))
#define ServoPwm4_MODE_MASK                      ((uint32)(ServoPwm4_3BIT_MASK        <<  \
                                                                            ServoPwm4_MODE_SHIFT))

/* Shift constants for trigger control register 1 */
#define ServoPwm4_CAPTURE_SHIFT                  (0u)
#define ServoPwm4_COUNT_SHIFT                    (2u)
#define ServoPwm4_RELOAD_SHIFT                   (4u)
#define ServoPwm4_STOP_SHIFT                     (6u)
#define ServoPwm4_START_SHIFT                    (8u)

/* Mask constants for trigger control register 1 */
#define ServoPwm4_CAPTURE_MASK                   ((uint32)(ServoPwm4_2BIT_MASK        <<  \
                                                                  ServoPwm4_CAPTURE_SHIFT))
#define ServoPwm4_COUNT_MASK                     ((uint32)(ServoPwm4_2BIT_MASK        <<  \
                                                                  ServoPwm4_COUNT_SHIFT))
#define ServoPwm4_RELOAD_MASK                    ((uint32)(ServoPwm4_2BIT_MASK        <<  \
                                                                  ServoPwm4_RELOAD_SHIFT))
#define ServoPwm4_STOP_MASK                      ((uint32)(ServoPwm4_2BIT_MASK        <<  \
                                                                  ServoPwm4_STOP_SHIFT))
#define ServoPwm4_START_MASK                     ((uint32)(ServoPwm4_2BIT_MASK        <<  \
                                                                  ServoPwm4_START_SHIFT))

/* MASK */
#define ServoPwm4_1BIT_MASK                      ((uint32)0x01u)
#define ServoPwm4_2BIT_MASK                      ((uint32)0x03u)
#define ServoPwm4_3BIT_MASK                      ((uint32)0x07u)
#define ServoPwm4_6BIT_MASK                      ((uint32)0x3Fu)
#define ServoPwm4_8BIT_MASK                      ((uint32)0xFFu)
#define ServoPwm4_16BIT_MASK                     ((uint32)0xFFFFu)

/* Shift constant for status register */
#define ServoPwm4_RUNNING_STATUS_SHIFT           (30u)


/***************************************
*    Initial Constants
***************************************/

#define ServoPwm4_CTRL_QUAD_BASE_CONFIG                                                          \
        (((uint32)(ServoPwm4_QUAD_ENCODING_MODES     << ServoPwm4_QUAD_MODE_SHIFT))       |\
         ((uint32)(ServoPwm4_CONFIG                  << ServoPwm4_MODE_SHIFT)))

#define ServoPwm4_CTRL_PWM_BASE_CONFIG                                                           \
        (((uint32)(ServoPwm4_PWM_STOP_EVENT          << ServoPwm4_PWM_STOP_KILL_SHIFT))   |\
         ((uint32)(ServoPwm4_PWM_OUT_INVERT          << ServoPwm4_INV_OUT_SHIFT))         |\
         ((uint32)(ServoPwm4_PWM_OUT_N_INVERT        << ServoPwm4_INV_COMPL_OUT_SHIFT))   |\
         ((uint32)(ServoPwm4_PWM_MODE                << ServoPwm4_MODE_SHIFT)))

#define ServoPwm4_CTRL_PWM_RUN_MODE                                                              \
            ((uint32)(ServoPwm4_PWM_RUN_MODE         << ServoPwm4_ONESHOT_SHIFT))
            
#define ServoPwm4_CTRL_PWM_ALIGN                                                                 \
            ((uint32)(ServoPwm4_PWM_ALIGN            << ServoPwm4_UPDOWN_SHIFT))

#define ServoPwm4_CTRL_PWM_KILL_EVENT                                                            \
             ((uint32)(ServoPwm4_PWM_KILL_EVENT      << ServoPwm4_PWM_SYNC_KILL_SHIFT))

#define ServoPwm4_CTRL_PWM_DEAD_TIME_CYCLE                                                       \
            ((uint32)(ServoPwm4_PWM_DEAD_TIME_CYCLE  << ServoPwm4_PRESCALER_SHIFT))

#define ServoPwm4_CTRL_PWM_PRESCALER                                                             \
            ((uint32)(ServoPwm4_PWM_PRESCALER        << ServoPwm4_PRESCALER_SHIFT))

#define ServoPwm4_CTRL_TIMER_BASE_CONFIG                                                         \
        (((uint32)(ServoPwm4_TC_PRESCALER            << ServoPwm4_PRESCALER_SHIFT))       |\
         ((uint32)(ServoPwm4_TC_COUNTER_MODE         << ServoPwm4_UPDOWN_SHIFT))          |\
         ((uint32)(ServoPwm4_TC_RUN_MODE             << ServoPwm4_ONESHOT_SHIFT))         |\
         ((uint32)(ServoPwm4_TC_COMP_CAP_MODE        << ServoPwm4_MODE_SHIFT)))
        
#define ServoPwm4_QUAD_SIGNALS_MODES                                                             \
        (((uint32)(ServoPwm4_QUAD_PHIA_SIGNAL_MODE   << ServoPwm4_COUNT_SHIFT))           |\
         ((uint32)(ServoPwm4_QUAD_INDEX_SIGNAL_MODE  << ServoPwm4_RELOAD_SHIFT))          |\
         ((uint32)(ServoPwm4_QUAD_STOP_SIGNAL_MODE   << ServoPwm4_STOP_SHIFT))            |\
         ((uint32)(ServoPwm4_QUAD_PHIB_SIGNAL_MODE   << ServoPwm4_START_SHIFT)))

#define ServoPwm4_PWM_SIGNALS_MODES                                                              \
        (((uint32)(ServoPwm4_PWM_SWITCH_SIGNAL_MODE  << ServoPwm4_CAPTURE_SHIFT))         |\
         ((uint32)(ServoPwm4_PWM_COUNT_SIGNAL_MODE   << ServoPwm4_COUNT_SHIFT))           |\
         ((uint32)(ServoPwm4_PWM_RELOAD_SIGNAL_MODE  << ServoPwm4_RELOAD_SHIFT))          |\
         ((uint32)(ServoPwm4_PWM_STOP_SIGNAL_MODE    << ServoPwm4_STOP_SHIFT))            |\
         ((uint32)(ServoPwm4_PWM_START_SIGNAL_MODE   << ServoPwm4_START_SHIFT)))

#define ServoPwm4_TIMER_SIGNALS_MODES                                                            \
        (((uint32)(ServoPwm4_TC_CAPTURE_SIGNAL_MODE  << ServoPwm4_CAPTURE_SHIFT))         |\
         ((uint32)(ServoPwm4_TC_COUNT_SIGNAL_MODE    << ServoPwm4_COUNT_SHIFT))           |\
         ((uint32)(ServoPwm4_TC_RELOAD_SIGNAL_MODE   << ServoPwm4_RELOAD_SHIFT))          |\
         ((uint32)(ServoPwm4_TC_STOP_SIGNAL_MODE     << ServoPwm4_STOP_SHIFT))            |\
         ((uint32)(ServoPwm4_TC_START_SIGNAL_MODE    << ServoPwm4_START_SHIFT)))
        
#define ServoPwm4_TIMER_UPDOWN_CNT_USED                                                          \
                ((ServoPwm4__COUNT_UPDOWN0 == ServoPwm4_TC_COUNTER_MODE)                  ||\
                 (ServoPwm4__COUNT_UPDOWN1 == ServoPwm4_TC_COUNTER_MODE))

#define ServoPwm4_PWM_UPDOWN_CNT_USED                                                            \
                ((ServoPwm4__CENTER == ServoPwm4_PWM_ALIGN)                               ||\
                 (ServoPwm4__ASYMMETRIC == ServoPwm4_PWM_ALIGN))               
        
#define ServoPwm4_PWM_PR_INIT_VALUE              (1u)
#define ServoPwm4_QUAD_PERIOD_INIT_VALUE         (0x8000u)



#endif /* End CY_TCPWM_ServoPwm4_H */

/* [] END OF FILE */
