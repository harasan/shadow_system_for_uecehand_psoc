/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

#ifndef HAND_H
    #define HAND_H
    
    void SetFourFingerFlexionExtensionSpeed(uint16 positionPerSeconds);
    uint16 GetFourFingerFlexionExtensionSpeed(void);
    uint8 GetFourFingerFlexionExtensionPosition(void);
    void SetFourFingerFlexionExtensionPosition(uint8 position);
    void RelaxFourFingerFlexionExtension(void);
    void StopFourFingerFlexionExtension(void);
    
    void SetThumbOppositionRepositionSpeed(uint16 positionPerSeconds);
    uint16 GetThumbOppositionRepositionSpeed(void);
    uint8 GetThumbOppositionRepositionPosition(void);
    void SetThumbOppositionRepositionPosition(uint8 position);
    void RelaxThumbOppositionReposition(void);
    void StopThumbOppositionReposition(void);
    
#endif

/* [] END OF FILE */
