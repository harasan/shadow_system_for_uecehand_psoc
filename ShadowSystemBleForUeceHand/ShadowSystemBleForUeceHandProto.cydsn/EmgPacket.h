/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

#ifndef PACKET_H
    #define PACKET_H
    //パケット全体の長さ(Byte)
    #define PACKET_LENGTH   200
    //パケットの内EMGデータの長さ(Byte)
    #define PACKET_EMG_LENGTH   180
    //EMGデータのひとまとまりの長さ(Byte) 12bit * 3ch * 2samples = 72bits
    #define PACKET_EMG_UNIT_LENGTH  9
    //パケット内EMGデータのまとまりの数
    #define PACKET_EMG_UNIT_COUNT   20
    //パケット内のフィードバックエリアの長さ(Byte)
    #define PACKET_FEEDBACK_LENGTH  20
    
    //取得された筋電信号と与えられたフィードバックをもとにパケット生成
    void CreatePacket(uint8* output, uint8* feedback);
#endif
/* [] END OF FILE */
