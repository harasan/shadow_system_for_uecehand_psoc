/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "EmgPacket.h"
#include "Adc.h"

static void CompressEMG(uint8* output);
static void SetFeedbackData(uint8* output, uint8* feedback);

void CreatePacket(uint8* output, uint8* feedback)
{
    CompressEMG(output);
    SetFeedbackData(output, feedback);
}

static void CompressEMG(uint8* output)
{
    uint32 cnt1, cnt2;
    uint8 buffer[PACKET_EMG_UNIT_LENGTH];
    uint32 *ptr;
    
    for(cnt1 = 0; cnt1 < PACKET_EMG_UNIT_COUNT; cnt1++)
    {
        for(cnt2 = 0; cnt2 < sizeof(buffer); cnt2++)
            buffer[cnt2] = 0;
        
        ptr = (uint32*)buffer;
        *ptr |= (uint32)((uint16)adcSamplesBuffer[0][cnt1 * 2] & 0x0fff);
        *ptr |= (uint32)((uint16)adcSamplesBuffer[1][cnt1 * 2] & 0x0fff) << ADC_RESOLUTION;
        
        ptr = (uint32*)(buffer + 3);
        *ptr |= (uint32)((uint16)adcSamplesBuffer[2][cnt1 * 2] & 0x0fff);
        *ptr |= (uint32)((uint16)adcSamplesBuffer[0][cnt1 * 2 + 1] & 0x0fff) << ADC_RESOLUTION;
        
        ptr = (uint32*)(buffer + 6);
        *ptr |= (uint32)((uint16)adcSamplesBuffer[1][cnt1 * 2 + 1] & 0x0fff);
        *ptr |= (uint32)((uint16)adcSamplesBuffer[2][cnt1 * 2 + 1] & 0x0fff) << ADC_RESOLUTION;
        
        memcpy(output + cnt1 * sizeof(buffer), buffer, sizeof(buffer));
    }
}

static void SetFeedbackData(uint8* output, uint8 feedback[])
{
    uint8* feedbackData = output + PACKET_EMG_LENGTH;
    if(feedback != NULL)
    {
        memcpy(feedbackData, feedback, PACKET_FEEDBACK_LENGTH);
    }
    else
    {
        for(int i = 0; i < PACKET_FEEDBACK_LENGTH; i++)
        {
            feedbackData[i] = 0;
        }
    }
}
/* [] END OF FILE */
