/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "Adc.h"
#include "EmgPacket.h"
#include "ServoMotor.h"

static void InitSystem(void);

int main(void)
{
    InitSystem();
    
    for(;;)
    {
        /* Place your application code here. */
        //LED1_Write(Button1_Read());
    }
}

static void InitSystem(void)
{
    CyGlobalIntEnable;
    InitAdc();
    StartAdc();
    InitServoMotor();
}

/* [] END OF FILE */
