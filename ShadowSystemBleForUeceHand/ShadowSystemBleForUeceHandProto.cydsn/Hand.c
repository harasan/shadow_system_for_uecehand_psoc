/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "project.h"
#include "Hand.h"
#include "ServoMotor.h"

#define FOUR_FINGER_FLEXION_EXTENSION_MOTOR_ID 1
#define THUMB_OPPOSITION_REPOSITION_MOTOR_ID 2

#define FOUR_FINGER_FLEXION_EXTENSION_MAX 255
#define FOUR_FINGER_FLEXION_EXTENSION_MIN 1
#define THUMB_OPPOSITION_REPOSITION_MAX 255
#define THUMB_OPPOSITION_REPOSITION_MIN 1

void SetFourFingerFlexionExtensionSpeed(uint16 positionPerSeconds)
{
    SetServoMotorSpeed(FOUR_FINGER_FLEXION_EXTENSION_MOTOR_ID, positionPerSeconds);
}

uint16 GetFourFingerFlexionExtensionSpeed(void)
{
    return GetServoMotorSpeed(FOUR_FINGER_FLEXION_EXTENSION_MOTOR_ID);
}

uint8 GetFourFingerFlexionExtensionPosition()
{
    return GetServoMotorPosition(FOUR_FINGER_FLEXION_EXTENSION_MOTOR_ID);
}

void SetFourFingerFlexionExtensionPosition(uint8 position)
{
    if(position > FOUR_FINGER_FLEXION_EXTENSION_MAX)
    {
        position = FOUR_FINGER_FLEXION_EXTENSION_MAX;
    }
    else if(position < FOUR_FINGER_FLEXION_EXTENSION_MIN)
    {
        position = FOUR_FINGER_FLEXION_EXTENSION_MIN;
    }
    
    SetServoMotorPositionWithSpeedLimit(
        FOUR_FINGER_FLEXION_EXTENSION_MOTOR_ID,
        position
    );
}

void RelaxFourFingerFlexionExtension(void)
{
    StopServoMotor(FOUR_FINGER_FLEXION_EXTENSION_MOTOR_ID);
}

void StopFourFingerFlexionExtension(void)
{
    KeepCurrentServoMotorPosition(FOUR_FINGER_FLEXION_EXTENSION_MOTOR_ID);
}

void SetThumbOppositionRepositionSpeed(uint16 positionPerSeconds)
{
    SetServoMotorSpeed(THUMB_OPPOSITION_REPOSITION_MOTOR_ID, positionPerSeconds);
}

uint16 GetThumbOppositionRepositionSpeed(void)
{
    return GetServoMotorSpeed(THUMB_OPPOSITION_REPOSITION_MOTOR_ID);
}

uint8 GetThumbOppositionRepositionPosition(void)
{
    return GetServoMotorPosition(THUMB_OPPOSITION_REPOSITION_MOTOR_ID);
}

void SetThumbOppositionRepositionPosition(uint8 position)
{
    if(position > THUMB_OPPOSITION_REPOSITION_MAX)
    {
        position = THUMB_OPPOSITION_REPOSITION_MAX;
    }
    else if(position < THUMB_OPPOSITION_REPOSITION_MIN)
    {
        position = THUMB_OPPOSITION_REPOSITION_MIN;
    }
    
    SetServoMotorPositionWithSpeedLimit(
        THUMB_OPPOSITION_REPOSITION_MOTOR_ID,
        position
    );
}

void RelaxThumbOppositionReposition(void)
{
    StopServoMotor(THUMB_OPPOSITION_REPOSITION_MOTOR_ID);
}

void StopThumbOppositionReposition(void)
{
    KeepCurrentServoMotorPosition(THUMB_OPPOSITION_REPOSITION_MOTOR_ID);
}

/* [] END OF FILE */
