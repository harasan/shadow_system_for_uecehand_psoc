/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "Adc.h"

int16 adcSamplesBuffer[ADC_CHANNEL_NUMBER][ADC_SAMPLE_COUNT];

static bool isSamplesReady;
static int16 adcSamples[ADC_CHANNEL_NUMBER][ADC_SAMPLE_COUNT];

static CY_ISR_PROTO(EmgDmaIsr);
static void InitEmgDma(void);

void InitAdc(void)
{
    CyIntEnable(CYDMA_INTR_NUMBER);
    EmgDma1_Start((void*)AdcSarSeq_SAR_CHAN0_RESULT_PTR, (void*)&adcSamples[0][0]);
    EmgDma2_Start((void*)AdcSarSeq_SAR_CHAN1_RESULT_PTR, (void*)&adcSamples[1][0]);
    EmgDma3_Start((void*)AdcSarSeq_SAR_CHAN2_RESULT_PTR, (void*)&adcSamples[2][0]);
    EmgDma1_SetInterruptCallback(EmgDmaIsr);
    
    Opamp_1_Start();
    Opamp_2_Start();
    Opamp_3_Start();
    Opamp_4_Start();
}

void StartAdc(void)
{
    InitEmgDma();
    
    AdcSarSeq_Start();
    
    AdcTrigger_Start();
}

void StopAdc(void)
{
    AdcTrigger_Stop();
}

bool GetIsAdcSamplesReady(void)
{
    return isSamplesReady;
}

void ResetIsAdcSamplesReady(void)
{
    isSamplesReady = false;
}

static CY_ISR(EmgDmaIsr)
{
    isSamplesReady = false;
    
    memcpy(adcSamplesBuffer, adcSamples, ADC_CHANNEL_NUMBER*ADC_SAMPLE_COUNT);
    
    InitEmgDma();
    
    isSamplesReady = true;
}

static void InitEmgDma(void)
{
    EmgDma1_SetSrcAddress(0, (void*)AdcSarSeq_SAR_CHAN0_RESULT_PTR);
    EmgDma1_SetDstAddress(0, (void*)&adcSamples[0][0]);
    EmgDma1_ValidateDescriptor(0);
    
    EmgDma2_SetSrcAddress(0, (void*)AdcSarSeq_SAR_CHAN1_RESULT_PTR);
    EmgDma2_SetDstAddress(0, (void*)&adcSamples[1][0]);
    EmgDma2_ValidateDescriptor(0);
    
    EmgDma3_SetSrcAddress(0, (void*)AdcSarSeq_SAR_CHAN2_RESULT_PTR);
    EmgDma3_SetDstAddress(0, (void*)&adcSamples[2][0]);
    EmgDma3_ValidateDescriptor(0);
}
/* [] END OF FILE */
