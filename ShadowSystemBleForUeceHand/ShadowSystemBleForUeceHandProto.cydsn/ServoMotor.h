/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include <stdbool.h>

//モータIDは1オリジン
#ifndef SERVO_MOTOR_H
    #define SERVO_MOTOR_H
    
    #define NUM_OF_SERVO_MOTOR 4
    
    void InitServoMotor();
    
    bool GetIsServoMotorMoving(uint8 motorId);
    
    uint16 GetServoMotorSpeed(uint8 motorId);
    void SetServoMotorSpeed(uint8 motorId, uint16 positionPerSeconds);
    
    uint8 GetServoMotorPosition(uint8 motorId);
    void SetServoMotorPosition(uint8 motorId, uint8 position);
    void SetServoMotorPositionWithSpeedLimit(uint8 motorId, uint8 position);
    
    void KeepCurrentServoMotorPosition(uint8 motorId);
    void StopServoMotor(uint8 motorId);
#endif
/* [] END OF FILE */
