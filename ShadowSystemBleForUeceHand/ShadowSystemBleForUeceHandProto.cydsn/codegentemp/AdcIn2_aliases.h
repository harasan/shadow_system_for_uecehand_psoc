/*******************************************************************************
* File Name: AdcIn2.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_AdcIn2_ALIASES_H) /* Pins AdcIn2_ALIASES_H */
#define CY_PINS_AdcIn2_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define AdcIn2_0			(AdcIn2__0__PC)
#define AdcIn2_0_PS		(AdcIn2__0__PS)
#define AdcIn2_0_PC		(AdcIn2__0__PC)
#define AdcIn2_0_DR		(AdcIn2__0__DR)
#define AdcIn2_0_SHIFT	(AdcIn2__0__SHIFT)
#define AdcIn2_0_INTR	((uint16)((uint16)0x0003u << (AdcIn2__0__SHIFT*2u)))

#define AdcIn2_INTR_ALL	 ((uint16)(AdcIn2_0_INTR))


#endif /* End Pins AdcIn2_ALIASES_H */


/* [] END OF FILE */
