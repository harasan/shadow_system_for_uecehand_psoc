/*******************************************************************************
* File Name: ServoPwm3.c
* Version 2.10
*
* Description:
*  This file provides the source code to the API for the ServoPwm3
*  component
*
* Note:
*  None
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "ServoPwm3.h"

uint8 ServoPwm3_initVar = 0u;


/*******************************************************************************
* Function Name: ServoPwm3_Init
********************************************************************************
*
* Summary:
*  Initialize/Restore default ServoPwm3 configuration.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_Init(void)
{

    /* Set values from customizer to CTRL */
    #if (ServoPwm3__QUAD == ServoPwm3_CONFIG)
        ServoPwm3_CONTROL_REG = ServoPwm3_CTRL_QUAD_BASE_CONFIG;
        
        /* Set values from customizer to CTRL1 */
        ServoPwm3_TRIG_CONTROL1_REG  = ServoPwm3_QUAD_SIGNALS_MODES;

        /* Set values from customizer to INTR */
        ServoPwm3_SetInterruptMode(ServoPwm3_QUAD_INTERRUPT_MASK);
        
         /* Set other values */
        ServoPwm3_SetCounterMode(ServoPwm3_COUNT_DOWN);
        ServoPwm3_WritePeriod(ServoPwm3_QUAD_PERIOD_INIT_VALUE);
        ServoPwm3_WriteCounter(ServoPwm3_QUAD_PERIOD_INIT_VALUE);
    #endif  /* (ServoPwm3__QUAD == ServoPwm3_CONFIG) */

    #if (ServoPwm3__TIMER == ServoPwm3_CONFIG)
        ServoPwm3_CONTROL_REG = ServoPwm3_CTRL_TIMER_BASE_CONFIG;
        
        /* Set values from customizer to CTRL1 */
        ServoPwm3_TRIG_CONTROL1_REG  = ServoPwm3_TIMER_SIGNALS_MODES;
    
        /* Set values from customizer to INTR */
        ServoPwm3_SetInterruptMode(ServoPwm3_TC_INTERRUPT_MASK);
        
        /* Set other values from customizer */
        ServoPwm3_WritePeriod(ServoPwm3_TC_PERIOD_VALUE );

        #if (ServoPwm3__COMPARE == ServoPwm3_TC_COMP_CAP_MODE)
            ServoPwm3_WriteCompare(ServoPwm3_TC_COMPARE_VALUE);

            #if (1u == ServoPwm3_TC_COMPARE_SWAP)
                ServoPwm3_SetCompareSwap(1u);
                ServoPwm3_WriteCompareBuf(ServoPwm3_TC_COMPARE_BUF_VALUE);
            #endif  /* (1u == ServoPwm3_TC_COMPARE_SWAP) */
        #endif  /* (ServoPwm3__COMPARE == ServoPwm3_TC_COMP_CAP_MODE) */

        /* Initialize counter value */
        #if (ServoPwm3_CY_TCPWM_V2 && ServoPwm3_TIMER_UPDOWN_CNT_USED && !ServoPwm3_CY_TCPWM_4000)
            ServoPwm3_WriteCounter(1u);
        #elif(ServoPwm3__COUNT_DOWN == ServoPwm3_TC_COUNTER_MODE)
            ServoPwm3_WriteCounter(ServoPwm3_TC_PERIOD_VALUE);
        #else
            ServoPwm3_WriteCounter(0u);
        #endif /* (ServoPwm3_CY_TCPWM_V2 && ServoPwm3_TIMER_UPDOWN_CNT_USED && !ServoPwm3_CY_TCPWM_4000) */
    #endif  /* (ServoPwm3__TIMER == ServoPwm3_CONFIG) */

    #if (ServoPwm3__PWM_SEL == ServoPwm3_CONFIG)
        ServoPwm3_CONTROL_REG = ServoPwm3_CTRL_PWM_BASE_CONFIG;

        #if (ServoPwm3__PWM_PR == ServoPwm3_PWM_MODE)
            ServoPwm3_CONTROL_REG |= ServoPwm3_CTRL_PWM_RUN_MODE;
            ServoPwm3_WriteCounter(ServoPwm3_PWM_PR_INIT_VALUE);
        #else
            ServoPwm3_CONTROL_REG |= ServoPwm3_CTRL_PWM_ALIGN | ServoPwm3_CTRL_PWM_KILL_EVENT;
            
            /* Initialize counter value */
            #if (ServoPwm3_CY_TCPWM_V2 && ServoPwm3_PWM_UPDOWN_CNT_USED && !ServoPwm3_CY_TCPWM_4000)
                ServoPwm3_WriteCounter(1u);
            #elif (ServoPwm3__RIGHT == ServoPwm3_PWM_ALIGN)
                ServoPwm3_WriteCounter(ServoPwm3_PWM_PERIOD_VALUE);
            #else 
                ServoPwm3_WriteCounter(0u);
            #endif  /* (ServoPwm3_CY_TCPWM_V2 && ServoPwm3_PWM_UPDOWN_CNT_USED && !ServoPwm3_CY_TCPWM_4000) */
        #endif  /* (ServoPwm3__PWM_PR == ServoPwm3_PWM_MODE) */

        #if (ServoPwm3__PWM_DT == ServoPwm3_PWM_MODE)
            ServoPwm3_CONTROL_REG |= ServoPwm3_CTRL_PWM_DEAD_TIME_CYCLE;
        #endif  /* (ServoPwm3__PWM_DT == ServoPwm3_PWM_MODE) */

        #if (ServoPwm3__PWM == ServoPwm3_PWM_MODE)
            ServoPwm3_CONTROL_REG |= ServoPwm3_CTRL_PWM_PRESCALER;
        #endif  /* (ServoPwm3__PWM == ServoPwm3_PWM_MODE) */

        /* Set values from customizer to CTRL1 */
        ServoPwm3_TRIG_CONTROL1_REG  = ServoPwm3_PWM_SIGNALS_MODES;
    
        /* Set values from customizer to INTR */
        ServoPwm3_SetInterruptMode(ServoPwm3_PWM_INTERRUPT_MASK);

        /* Set values from customizer to CTRL2 */
        #if (ServoPwm3__PWM_PR == ServoPwm3_PWM_MODE)
            ServoPwm3_TRIG_CONTROL2_REG =
                    (ServoPwm3_CC_MATCH_NO_CHANGE    |
                    ServoPwm3_OVERLOW_NO_CHANGE      |
                    ServoPwm3_UNDERFLOW_NO_CHANGE);
        #else
            #if (ServoPwm3__LEFT == ServoPwm3_PWM_ALIGN)
                ServoPwm3_TRIG_CONTROL2_REG = ServoPwm3_PWM_MODE_LEFT;
            #endif  /* ( ServoPwm3_PWM_LEFT == ServoPwm3_PWM_ALIGN) */

            #if (ServoPwm3__RIGHT == ServoPwm3_PWM_ALIGN)
                ServoPwm3_TRIG_CONTROL2_REG = ServoPwm3_PWM_MODE_RIGHT;
            #endif  /* ( ServoPwm3_PWM_RIGHT == ServoPwm3_PWM_ALIGN) */

            #if (ServoPwm3__CENTER == ServoPwm3_PWM_ALIGN)
                ServoPwm3_TRIG_CONTROL2_REG = ServoPwm3_PWM_MODE_CENTER;
            #endif  /* ( ServoPwm3_PWM_CENTER == ServoPwm3_PWM_ALIGN) */

            #if (ServoPwm3__ASYMMETRIC == ServoPwm3_PWM_ALIGN)
                ServoPwm3_TRIG_CONTROL2_REG = ServoPwm3_PWM_MODE_ASYM;
            #endif  /* (ServoPwm3__ASYMMETRIC == ServoPwm3_PWM_ALIGN) */
        #endif  /* (ServoPwm3__PWM_PR == ServoPwm3_PWM_MODE) */

        /* Set other values from customizer */
        ServoPwm3_WritePeriod(ServoPwm3_PWM_PERIOD_VALUE );
        ServoPwm3_WriteCompare(ServoPwm3_PWM_COMPARE_VALUE);

        #if (1u == ServoPwm3_PWM_COMPARE_SWAP)
            ServoPwm3_SetCompareSwap(1u);
            ServoPwm3_WriteCompareBuf(ServoPwm3_PWM_COMPARE_BUF_VALUE);
        #endif  /* (1u == ServoPwm3_PWM_COMPARE_SWAP) */

        #if (1u == ServoPwm3_PWM_PERIOD_SWAP)
            ServoPwm3_SetPeriodSwap(1u);
            ServoPwm3_WritePeriodBuf(ServoPwm3_PWM_PERIOD_BUF_VALUE);
        #endif  /* (1u == ServoPwm3_PWM_PERIOD_SWAP) */
    #endif  /* (ServoPwm3__PWM_SEL == ServoPwm3_CONFIG) */
    
}


/*******************************************************************************
* Function Name: ServoPwm3_Enable
********************************************************************************
*
* Summary:
*  Enables the ServoPwm3.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_Enable(void)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();
    ServoPwm3_BLOCK_CONTROL_REG |= ServoPwm3_MASK;
    CyExitCriticalSection(enableInterrupts);

    /* Start Timer or PWM if start input is absent */
    #if (ServoPwm3__PWM_SEL == ServoPwm3_CONFIG)
        #if (0u == ServoPwm3_PWM_START_SIGNAL_PRESENT)
            ServoPwm3_TriggerCommand(ServoPwm3_MASK, ServoPwm3_CMD_START);
        #endif /* (0u == ServoPwm3_PWM_START_SIGNAL_PRESENT) */
    #endif /* (ServoPwm3__PWM_SEL == ServoPwm3_CONFIG) */

    #if (ServoPwm3__TIMER == ServoPwm3_CONFIG)
        #if (0u == ServoPwm3_TC_START_SIGNAL_PRESENT)
            ServoPwm3_TriggerCommand(ServoPwm3_MASK, ServoPwm3_CMD_START);
        #endif /* (0u == ServoPwm3_TC_START_SIGNAL_PRESENT) */
    #endif /* (ServoPwm3__TIMER == ServoPwm3_CONFIG) */
    
    #if (ServoPwm3__QUAD == ServoPwm3_CONFIG)
        #if (0u != ServoPwm3_QUAD_AUTO_START)
            ServoPwm3_TriggerCommand(ServoPwm3_MASK, ServoPwm3_CMD_RELOAD);
        #endif /* (0u != ServoPwm3_QUAD_AUTO_START) */
    #endif  /* (ServoPwm3__QUAD == ServoPwm3_CONFIG) */
}


/*******************************************************************************
* Function Name: ServoPwm3_Start
********************************************************************************
*
* Summary:
*  Initializes the ServoPwm3 with default customizer
*  values when called the first time and enables the ServoPwm3.
*  For subsequent calls the configuration is left unchanged and the component is
*  just enabled.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  ServoPwm3_initVar: global variable is used to indicate initial
*  configuration of this component.  The variable is initialized to zero and set
*  to 1 the first time ServoPwm3_Start() is called. This allows
*  enabling/disabling a component without re-initialization in all subsequent
*  calls to the ServoPwm3_Start() routine.
*
*******************************************************************************/
void ServoPwm3_Start(void)
{
    if (0u == ServoPwm3_initVar)
    {
        ServoPwm3_Init();
        ServoPwm3_initVar = 1u;
    }

    ServoPwm3_Enable();
}


/*******************************************************************************
* Function Name: ServoPwm3_Stop
********************************************************************************
*
* Summary:
*  Disables the ServoPwm3.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_Stop(void)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();

    ServoPwm3_BLOCK_CONTROL_REG &= (uint32)~ServoPwm3_MASK;

    CyExitCriticalSection(enableInterrupts);
}


/*******************************************************************************
* Function Name: ServoPwm3_SetMode
********************************************************************************
*
* Summary:
*  Sets the operation mode of the ServoPwm3. This function is used when
*  configured as a generic ServoPwm3 and the actual mode of operation is
*  set at runtime. The mode must be set while the component is disabled.
*
* Parameters:
*  mode: Mode for the ServoPwm3 to operate in
*   Values:
*   - ServoPwm3_MODE_TIMER_COMPARE - Timer / Counter with
*                                                 compare capability
*         - ServoPwm3_MODE_TIMER_CAPTURE - Timer / Counter with
*                                                 capture capability
*         - ServoPwm3_MODE_QUAD - Quadrature decoder
*         - ServoPwm3_MODE_PWM - PWM
*         - ServoPwm3_MODE_PWM_DT - PWM with dead time
*         - ServoPwm3_MODE_PWM_PR - PWM with pseudo random capability
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_SetMode(uint32 mode)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();

    ServoPwm3_CONTROL_REG &= (uint32)~ServoPwm3_MODE_MASK;
    ServoPwm3_CONTROL_REG |= mode;

    CyExitCriticalSection(enableInterrupts);
}


/*******************************************************************************
* Function Name: ServoPwm3_SetQDMode
********************************************************************************
*
* Summary:
*  Sets the the Quadrature Decoder to one of the 3 supported modes.
*  Its functionality is only applicable to Quadrature Decoder operation.
*
* Parameters:
*  qdMode: Quadrature Decoder mode
*   Values:
*         - ServoPwm3_MODE_X1 - Counts on phi 1 rising
*         - ServoPwm3_MODE_X2 - Counts on both edges of phi1 (2x faster)
*         - ServoPwm3_MODE_X4 - Counts on both edges of phi1 and phi2
*                                        (4x faster)
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_SetQDMode(uint32 qdMode)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();

    ServoPwm3_CONTROL_REG &= (uint32)~ServoPwm3_QUAD_MODE_MASK;
    ServoPwm3_CONTROL_REG |= qdMode;

    CyExitCriticalSection(enableInterrupts);
}


/*******************************************************************************
* Function Name: ServoPwm3_SetPrescaler
********************************************************************************
*
* Summary:
*  Sets the prescaler value that is applied to the clock input.  Not applicable
*  to a PWM with the dead time mode or Quadrature Decoder mode.
*
* Parameters:
*  prescaler: Prescaler divider value
*   Values:
*         - ServoPwm3_PRESCALE_DIVBY1    - Divide by 1 (no prescaling)
*         - ServoPwm3_PRESCALE_DIVBY2    - Divide by 2
*         - ServoPwm3_PRESCALE_DIVBY4    - Divide by 4
*         - ServoPwm3_PRESCALE_DIVBY8    - Divide by 8
*         - ServoPwm3_PRESCALE_DIVBY16   - Divide by 16
*         - ServoPwm3_PRESCALE_DIVBY32   - Divide by 32
*         - ServoPwm3_PRESCALE_DIVBY64   - Divide by 64
*         - ServoPwm3_PRESCALE_DIVBY128  - Divide by 128
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_SetPrescaler(uint32 prescaler)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();

    ServoPwm3_CONTROL_REG &= (uint32)~ServoPwm3_PRESCALER_MASK;
    ServoPwm3_CONTROL_REG |= prescaler;

    CyExitCriticalSection(enableInterrupts);
}


/*******************************************************************************
* Function Name: ServoPwm3_SetOneShot
********************************************************************************
*
* Summary:
*  Writes the register that controls whether the ServoPwm3 runs
*  continuously or stops when terminal count is reached.  By default the
*  ServoPwm3 operates in the continuous mode.
*
* Parameters:
*  oneShotEnable
*   Values:
*     - 0 - Continuous
*     - 1 - Enable One Shot
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_SetOneShot(uint32 oneShotEnable)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();

    ServoPwm3_CONTROL_REG &= (uint32)~ServoPwm3_ONESHOT_MASK;
    ServoPwm3_CONTROL_REG |= ((uint32)((oneShotEnable & ServoPwm3_1BIT_MASK) <<
                                                               ServoPwm3_ONESHOT_SHIFT));

    CyExitCriticalSection(enableInterrupts);
}


/*******************************************************************************
* Function Name: ServoPwm3_SetPWMMode
********************************************************************************
*
* Summary:
*  Writes the control register that determines what mode of operation the PWM
*  output lines are driven in.  There is a setting for what to do on a
*  comparison match (CC_MATCH), on an overflow (OVERFLOW) and on an underflow
*  (UNDERFLOW).  The value for each of the three must be ORed together to form
*  the mode.
*
* Parameters:
*  modeMask: A combination of three mode settings.  Mask must include a value
*  for each of the three or use one of the preconfigured PWM settings.
*   Values:
*     - CC_MATCH_SET        - Set on comparison match
*     - CC_MATCH_CLEAR      - Clear on comparison match
*     - CC_MATCH_INVERT     - Invert on comparison match
*     - CC_MATCH_NO_CHANGE  - No change on comparison match
*     - OVERLOW_SET         - Set on overflow
*     - OVERLOW_CLEAR       - Clear on  overflow
*     - OVERLOW_INVERT      - Invert on overflow
*     - OVERLOW_NO_CHANGE   - No change on overflow
*     - UNDERFLOW_SET       - Set on underflow
*     - UNDERFLOW_CLEAR     - Clear on underflow
*     - UNDERFLOW_INVERT    - Invert on underflow
*     - UNDERFLOW_NO_CHANGE - No change on underflow
*     - PWM_MODE_LEFT       - Setting for left aligned PWM.  Should be combined
*                             with up counting mode
*     - PWM_MODE_RIGHT      - Setting for right aligned PWM.  Should be combined
*                             with down counting mode
*     - PWM_MODE_CENTER     - Setting for center aligned PWM.  Should be
*                             combined with up/down 0 mode
*     - PWM_MODE_ASYM       - Setting for asymmetric PWM.  Should be combined
*                             with up/down 1 mode
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_SetPWMMode(uint32 modeMask)
{
    ServoPwm3_TRIG_CONTROL2_REG = (modeMask & ServoPwm3_6BIT_MASK);
}



/*******************************************************************************
* Function Name: ServoPwm3_SetPWMSyncKill
********************************************************************************
*
* Summary:
*  Writes the register that controls whether the PWM kill signal (stop input)
*  causes asynchronous or synchronous kill operation.  By default the kill
*  operation is asynchronous.  This functionality is only applicable to the PWM
*  and PWM with dead time modes.
*
*  For Synchronous mode the kill signal disables both the line and line_n
*  signals until the next terminal count.
*
*  For Asynchronous mode the kill signal disables both the line and line_n
*  signals when the kill signal is present.  This mode should only be used
*  when the kill signal (stop input) is configured in the pass through mode
*  (Level sensitive signal).

*
* Parameters:
*  syncKillEnable
*   Values:
*     - 0 - Asynchronous
*     - 1 - Synchronous
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_SetPWMSyncKill(uint32 syncKillEnable)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();

    ServoPwm3_CONTROL_REG &= (uint32)~ServoPwm3_PWM_SYNC_KILL_MASK;
    ServoPwm3_CONTROL_REG |= ((uint32)((syncKillEnable & ServoPwm3_1BIT_MASK)  <<
                                               ServoPwm3_PWM_SYNC_KILL_SHIFT));

    CyExitCriticalSection(enableInterrupts);
}


/*******************************************************************************
* Function Name: ServoPwm3_SetPWMStopOnKill
********************************************************************************
*
* Summary:
*  Writes the register that controls whether the PWM kill signal (stop input)
*  causes the PWM counter to stop.  By default the kill operation does not stop
*  the counter.  This functionality is only applicable to the three PWM modes.
*
*
* Parameters:
*  stopOnKillEnable
*   Values:
*     - 0 - Don't stop
*     - 1 - Stop
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_SetPWMStopOnKill(uint32 stopOnKillEnable)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();

    ServoPwm3_CONTROL_REG &= (uint32)~ServoPwm3_PWM_STOP_KILL_MASK;
    ServoPwm3_CONTROL_REG |= ((uint32)((stopOnKillEnable & ServoPwm3_1BIT_MASK)  <<
                                                         ServoPwm3_PWM_STOP_KILL_SHIFT));

    CyExitCriticalSection(enableInterrupts);
}


/*******************************************************************************
* Function Name: ServoPwm3_SetPWMDeadTime
********************************************************************************
*
* Summary:
*  Writes the dead time control value.  This value delays the rising edge of
*  both the line and line_n signals the designated number of cycles resulting
*  in both signals being inactive for that many cycles.  This functionality is
*  only applicable to the PWM in the dead time mode.

*
* Parameters:
*  Dead time to insert
*   Values: 0 to 255
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_SetPWMDeadTime(uint32 deadTime)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();

    ServoPwm3_CONTROL_REG &= (uint32)~ServoPwm3_PRESCALER_MASK;
    ServoPwm3_CONTROL_REG |= ((uint32)((deadTime & ServoPwm3_8BIT_MASK) <<
                                                          ServoPwm3_PRESCALER_SHIFT));

    CyExitCriticalSection(enableInterrupts);
}


/*******************************************************************************
* Function Name: ServoPwm3_SetPWMInvert
********************************************************************************
*
* Summary:
*  Writes the bits that control whether the line and line_n outputs are
*  inverted from their normal output values.  This functionality is only
*  applicable to the three PWM modes.
*
* Parameters:
*  mask: Mask of outputs to invert.
*   Values:
*         - ServoPwm3_INVERT_LINE   - Inverts the line output
*         - ServoPwm3_INVERT_LINE_N - Inverts the line_n output
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_SetPWMInvert(uint32 mask)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();

    ServoPwm3_CONTROL_REG &= (uint32)~ServoPwm3_INV_OUT_MASK;
    ServoPwm3_CONTROL_REG |= mask;

    CyExitCriticalSection(enableInterrupts);
}



/*******************************************************************************
* Function Name: ServoPwm3_WriteCounter
********************************************************************************
*
* Summary:
*  Writes a new 16bit counter value directly into the counter register, thus
*  setting the counter (not the period) to the value written. It is not
*  advised to write to this field when the counter is running.
*
* Parameters:
*  count: value to write
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_WriteCounter(uint32 count)
{
    ServoPwm3_COUNTER_REG = (count & ServoPwm3_16BIT_MASK);
}


/*******************************************************************************
* Function Name: ServoPwm3_ReadCounter
********************************************************************************
*
* Summary:
*  Reads the current counter value.
*
* Parameters:
*  None
*
* Return:
*  Current counter value
*
*******************************************************************************/
uint32 ServoPwm3_ReadCounter(void)
{
    return (ServoPwm3_COUNTER_REG & ServoPwm3_16BIT_MASK);
}


/*******************************************************************************
* Function Name: ServoPwm3_SetCounterMode
********************************************************************************
*
* Summary:
*  Sets the counter mode.  Applicable to all modes except Quadrature Decoder
*  and the PWM with a pseudo random output.
*
* Parameters:
*  counterMode: Enumerated counter type values
*   Values:
*     - ServoPwm3_COUNT_UP       - Counts up
*     - ServoPwm3_COUNT_DOWN     - Counts down
*     - ServoPwm3_COUNT_UPDOWN0  - Counts up and down. Terminal count
*                                         generated when counter reaches 0
*     - ServoPwm3_COUNT_UPDOWN1  - Counts up and down. Terminal count
*                                         generated both when counter reaches 0
*                                         and period
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_SetCounterMode(uint32 counterMode)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();

    ServoPwm3_CONTROL_REG &= (uint32)~ServoPwm3_UPDOWN_MASK;
    ServoPwm3_CONTROL_REG |= counterMode;

    CyExitCriticalSection(enableInterrupts);
}


/*******************************************************************************
* Function Name: ServoPwm3_WritePeriod
********************************************************************************
*
* Summary:
*  Writes the 16 bit period register with the new period value.
*  To cause the counter to count for N cycles this register should be written
*  with N-1 (counts from 0 to period inclusive).
*
* Parameters:
*  period: Period value
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_WritePeriod(uint32 period)
{
    ServoPwm3_PERIOD_REG = (period & ServoPwm3_16BIT_MASK);
}


/*******************************************************************************
* Function Name: ServoPwm3_ReadPeriod
********************************************************************************
*
* Summary:
*  Reads the 16 bit period register.
*
* Parameters:
*  None
*
* Return:
*  Period value
*
*******************************************************************************/
uint32 ServoPwm3_ReadPeriod(void)
{
    return (ServoPwm3_PERIOD_REG & ServoPwm3_16BIT_MASK);
}


/*******************************************************************************
* Function Name: ServoPwm3_SetCompareSwap
********************************************************************************
*
* Summary:
*  Writes the register that controls whether the compare registers are
*  swapped. When enabled in the Timer/Counter mode(without capture) the swap
*  occurs at a TC event. In the PWM mode the swap occurs at the next TC event
*  following a hardware switch event.
*
* Parameters:
*  swapEnable
*   Values:
*     - 0 - Disable swap
*     - 1 - Enable swap
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_SetCompareSwap(uint32 swapEnable)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();

    ServoPwm3_CONTROL_REG &= (uint32)~ServoPwm3_RELOAD_CC_MASK;
    ServoPwm3_CONTROL_REG |= (swapEnable & ServoPwm3_1BIT_MASK);

    CyExitCriticalSection(enableInterrupts);
}


/*******************************************************************************
* Function Name: ServoPwm3_WritePeriodBuf
********************************************************************************
*
* Summary:
*  Writes the 16 bit period buf register with the new period value.
*
* Parameters:
*  periodBuf: Period value
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_WritePeriodBuf(uint32 periodBuf)
{
    ServoPwm3_PERIOD_BUF_REG = (periodBuf & ServoPwm3_16BIT_MASK);
}


/*******************************************************************************
* Function Name: ServoPwm3_ReadPeriodBuf
********************************************************************************
*
* Summary:
*  Reads the 16 bit period buf register.
*
* Parameters:
*  None
*
* Return:
*  Period value
*
*******************************************************************************/
uint32 ServoPwm3_ReadPeriodBuf(void)
{
    return (ServoPwm3_PERIOD_BUF_REG & ServoPwm3_16BIT_MASK);
}


/*******************************************************************************
* Function Name: ServoPwm3_SetPeriodSwap
********************************************************************************
*
* Summary:
*  Writes the register that controls whether the period registers are
*  swapped. When enabled in Timer/Counter mode the swap occurs at a TC event.
*  In the PWM mode the swap occurs at the next TC event following a hardware
*  switch event.
*
* Parameters:
*  swapEnable
*   Values:
*     - 0 - Disable swap
*     - 1 - Enable swap
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_SetPeriodSwap(uint32 swapEnable)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();

    ServoPwm3_CONTROL_REG &= (uint32)~ServoPwm3_RELOAD_PERIOD_MASK;
    ServoPwm3_CONTROL_REG |= ((uint32)((swapEnable & ServoPwm3_1BIT_MASK) <<
                                                            ServoPwm3_RELOAD_PERIOD_SHIFT));

    CyExitCriticalSection(enableInterrupts);
}


/*******************************************************************************
* Function Name: ServoPwm3_WriteCompare
********************************************************************************
*
* Summary:
*  Writes the 16 bit compare register with the new compare value. Not
*  applicable for Timer/Counter with Capture or in Quadrature Decoder modes.
*
* Parameters:
*  compare: Compare value
*
* Return:
*  None
*
* Note:
*  It is not recommended to use the value equal to "0" or equal to 
*  "period value" in Center or Asymmetric align PWM modes on the 
*  PSoC 4100/PSoC 4200 devices.
*  PSoC 4000 devices write the 16 bit compare register with the decremented 
*  compare value in the Up counting mode (except 0x0u), and the incremented 
*  compare value in the Down counting mode (except 0xFFFFu).
*
*******************************************************************************/
void ServoPwm3_WriteCompare(uint32 compare)
{
    #if (ServoPwm3_CY_TCPWM_4000)
        uint32 currentMode;
    #endif /* (ServoPwm3_CY_TCPWM_4000) */

    #if (ServoPwm3_CY_TCPWM_4000)
        currentMode = ((ServoPwm3_CONTROL_REG & ServoPwm3_UPDOWN_MASK) >> ServoPwm3_UPDOWN_SHIFT);

        if (((uint32)ServoPwm3__COUNT_DOWN == currentMode) && (0xFFFFu != compare))
        {
            compare++;
        }
        else if (((uint32)ServoPwm3__COUNT_UP == currentMode) && (0u != compare))
        {
            compare--;
        }
        else
        {
        }
        
    
    #endif /* (ServoPwm3_CY_TCPWM_4000) */
    
    ServoPwm3_COMP_CAP_REG = (compare & ServoPwm3_16BIT_MASK);
}


/*******************************************************************************
* Function Name: ServoPwm3_ReadCompare
********************************************************************************
*
* Summary:
*  Reads the compare register. Not applicable for Timer/Counter with Capture
*  or in Quadrature Decoder modes.
*  PSoC 4000 devices read the incremented compare register value in the 
*  Up counting mode (except 0xFFFFu), and the decremented value in the 
*  Down counting mode (except 0x0u).
*
* Parameters:
*  None
*
* Return:
*  Compare value
*
* Note:
*  PSoC 4000 devices read the incremented compare register value in the 
*  Up counting mode (except 0xFFFFu), and the decremented value in the 
*  Down counting mode (except 0x0u).
*
*******************************************************************************/
uint32 ServoPwm3_ReadCompare(void)
{
    #if (ServoPwm3_CY_TCPWM_4000)
        uint32 currentMode;
        uint32 regVal;
    #endif /* (ServoPwm3_CY_TCPWM_4000) */

    #if (ServoPwm3_CY_TCPWM_4000)
        currentMode = ((ServoPwm3_CONTROL_REG & ServoPwm3_UPDOWN_MASK) >> ServoPwm3_UPDOWN_SHIFT);
        
        regVal = ServoPwm3_COMP_CAP_REG;
        
        if (((uint32)ServoPwm3__COUNT_DOWN == currentMode) && (0u != regVal))
        {
            regVal--;
        }
        else if (((uint32)ServoPwm3__COUNT_UP == currentMode) && (0xFFFFu != regVal))
        {
            regVal++;
        }
        else
        {
        }

        return (regVal & ServoPwm3_16BIT_MASK);
    #else
        return (ServoPwm3_COMP_CAP_REG & ServoPwm3_16BIT_MASK);
    #endif /* (ServoPwm3_CY_TCPWM_4000) */
}


/*******************************************************************************
* Function Name: ServoPwm3_WriteCompareBuf
********************************************************************************
*
* Summary:
*  Writes the 16 bit compare buffer register with the new compare value. Not
*  applicable for Timer/Counter with Capture or in Quadrature Decoder modes.
*
* Parameters:
*  compareBuf: Compare value
*
* Return:
*  None
*
* Note:
*  It is not recommended to use the value equal to "0" or equal to 
*  "period value" in Center or Asymmetric align PWM modes on the 
*  PSoC 4100/PSoC 4200 devices.
*  PSoC 4000 devices write the 16 bit compare register with the decremented 
*  compare value in the Up counting mode (except 0x0u), and the incremented 
*  compare value in the Down counting mode (except 0xFFFFu).
*
*******************************************************************************/
void ServoPwm3_WriteCompareBuf(uint32 compareBuf)
{
    #if (ServoPwm3_CY_TCPWM_4000)
        uint32 currentMode;
    #endif /* (ServoPwm3_CY_TCPWM_4000) */

    #if (ServoPwm3_CY_TCPWM_4000)
        currentMode = ((ServoPwm3_CONTROL_REG & ServoPwm3_UPDOWN_MASK) >> ServoPwm3_UPDOWN_SHIFT);

        if (((uint32)ServoPwm3__COUNT_DOWN == currentMode) && (0xFFFFu != compareBuf))
        {
            compareBuf++;
        }
        else if (((uint32)ServoPwm3__COUNT_UP == currentMode) && (0u != compareBuf))
        {
            compareBuf --;
        }
        else
        {
        }
    #endif /* (ServoPwm3_CY_TCPWM_4000) */
    
    ServoPwm3_COMP_CAP_BUF_REG = (compareBuf & ServoPwm3_16BIT_MASK);
}


/*******************************************************************************
* Function Name: ServoPwm3_ReadCompareBuf
********************************************************************************
*
* Summary:
*  Reads the compare buffer register. Not applicable for Timer/Counter with
*  Capture or in Quadrature Decoder modes.
*
* Parameters:
*  None
*
* Return:
*  Compare buffer value
*
* Note:
*  PSoC 4000 devices read the incremented compare register value in the 
*  Up counting mode (except 0xFFFFu), and the decremented value in the 
*  Down counting mode (except 0x0u).
*
*******************************************************************************/
uint32 ServoPwm3_ReadCompareBuf(void)
{
    #if (ServoPwm3_CY_TCPWM_4000)
        uint32 currentMode;
        uint32 regVal;
    #endif /* (ServoPwm3_CY_TCPWM_4000) */

    #if (ServoPwm3_CY_TCPWM_4000)
        currentMode = ((ServoPwm3_CONTROL_REG & ServoPwm3_UPDOWN_MASK) >> ServoPwm3_UPDOWN_SHIFT);

        regVal = ServoPwm3_COMP_CAP_BUF_REG;
        
        if (((uint32)ServoPwm3__COUNT_DOWN == currentMode) && (0u != regVal))
        {
            regVal--;
        }
        else if (((uint32)ServoPwm3__COUNT_UP == currentMode) && (0xFFFFu != regVal))
        {
            regVal++;
        }
        else
        {
        }

        return (regVal & ServoPwm3_16BIT_MASK);
    #else
        return (ServoPwm3_COMP_CAP_BUF_REG & ServoPwm3_16BIT_MASK);
    #endif /* (ServoPwm3_CY_TCPWM_4000) */
}


/*******************************************************************************
* Function Name: ServoPwm3_ReadCapture
********************************************************************************
*
* Summary:
*  Reads the captured counter value. This API is applicable only for
*  Timer/Counter with the capture mode and Quadrature Decoder modes.
*
* Parameters:
*  None
*
* Return:
*  Capture value
*
*******************************************************************************/
uint32 ServoPwm3_ReadCapture(void)
{
    return (ServoPwm3_COMP_CAP_REG & ServoPwm3_16BIT_MASK);
}


/*******************************************************************************
* Function Name: ServoPwm3_ReadCaptureBuf
********************************************************************************
*
* Summary:
*  Reads the capture buffer register. This API is applicable only for
*  Timer/Counter with the capture mode and Quadrature Decoder modes.
*
* Parameters:
*  None
*
* Return:
*  Capture buffer value
*
*******************************************************************************/
uint32 ServoPwm3_ReadCaptureBuf(void)
{
    return (ServoPwm3_COMP_CAP_BUF_REG & ServoPwm3_16BIT_MASK);
}


/*******************************************************************************
* Function Name: ServoPwm3_SetCaptureMode
********************************************************************************
*
* Summary:
*  Sets the capture trigger mode. For PWM mode this is the switch input.
*  This input is not applicable to the Timer/Counter without Capture and
*  Quadrature Decoder modes.
*
* Parameters:
*  triggerMode: Enumerated trigger mode value
*   Values:
*     - ServoPwm3_TRIG_LEVEL     - Level
*     - ServoPwm3_TRIG_RISING    - Rising edge
*     - ServoPwm3_TRIG_FALLING   - Falling edge
*     - ServoPwm3_TRIG_BOTH      - Both rising and falling edge
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_SetCaptureMode(uint32 triggerMode)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();

    ServoPwm3_TRIG_CONTROL1_REG &= (uint32)~ServoPwm3_CAPTURE_MASK;
    ServoPwm3_TRIG_CONTROL1_REG |= triggerMode;

    CyExitCriticalSection(enableInterrupts);
}


/*******************************************************************************
* Function Name: ServoPwm3_SetReloadMode
********************************************************************************
*
* Summary:
*  Sets the reload trigger mode. For Quadrature Decoder mode this is the index
*  input.
*
* Parameters:
*  triggerMode: Enumerated trigger mode value
*   Values:
*     - ServoPwm3_TRIG_LEVEL     - Level
*     - ServoPwm3_TRIG_RISING    - Rising edge
*     - ServoPwm3_TRIG_FALLING   - Falling edge
*     - ServoPwm3_TRIG_BOTH      - Both rising and falling edge
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_SetReloadMode(uint32 triggerMode)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();

    ServoPwm3_TRIG_CONTROL1_REG &= (uint32)~ServoPwm3_RELOAD_MASK;
    ServoPwm3_TRIG_CONTROL1_REG |= ((uint32)(triggerMode << ServoPwm3_RELOAD_SHIFT));

    CyExitCriticalSection(enableInterrupts);
}


/*******************************************************************************
* Function Name: ServoPwm3_SetStartMode
********************************************************************************
*
* Summary:
*  Sets the start trigger mode. For Quadrature Decoder mode this is the
*  phiB input.
*
* Parameters:
*  triggerMode: Enumerated trigger mode value
*   Values:
*     - ServoPwm3_TRIG_LEVEL     - Level
*     - ServoPwm3_TRIG_RISING    - Rising edge
*     - ServoPwm3_TRIG_FALLING   - Falling edge
*     - ServoPwm3_TRIG_BOTH      - Both rising and falling edge
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_SetStartMode(uint32 triggerMode)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();

    ServoPwm3_TRIG_CONTROL1_REG &= (uint32)~ServoPwm3_START_MASK;
    ServoPwm3_TRIG_CONTROL1_REG |= ((uint32)(triggerMode << ServoPwm3_START_SHIFT));

    CyExitCriticalSection(enableInterrupts);
}


/*******************************************************************************
* Function Name: ServoPwm3_SetStopMode
********************************************************************************
*
* Summary:
*  Sets the stop trigger mode. For PWM mode this is the kill input.
*
* Parameters:
*  triggerMode: Enumerated trigger mode value
*   Values:
*     - ServoPwm3_TRIG_LEVEL     - Level
*     - ServoPwm3_TRIG_RISING    - Rising edge
*     - ServoPwm3_TRIG_FALLING   - Falling edge
*     - ServoPwm3_TRIG_BOTH      - Both rising and falling edge
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_SetStopMode(uint32 triggerMode)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();

    ServoPwm3_TRIG_CONTROL1_REG &= (uint32)~ServoPwm3_STOP_MASK;
    ServoPwm3_TRIG_CONTROL1_REG |= ((uint32)(triggerMode << ServoPwm3_STOP_SHIFT));

    CyExitCriticalSection(enableInterrupts);
}


/*******************************************************************************
* Function Name: ServoPwm3_SetCountMode
********************************************************************************
*
* Summary:
*  Sets the count trigger mode. For Quadrature Decoder mode this is the phiA
*  input.
*
* Parameters:
*  triggerMode: Enumerated trigger mode value
*   Values:
*     - ServoPwm3_TRIG_LEVEL     - Level
*     - ServoPwm3_TRIG_RISING    - Rising edge
*     - ServoPwm3_TRIG_FALLING   - Falling edge
*     - ServoPwm3_TRIG_BOTH      - Both rising and falling edge
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_SetCountMode(uint32 triggerMode)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();

    ServoPwm3_TRIG_CONTROL1_REG &= (uint32)~ServoPwm3_COUNT_MASK;
    ServoPwm3_TRIG_CONTROL1_REG |= ((uint32)(triggerMode << ServoPwm3_COUNT_SHIFT));

    CyExitCriticalSection(enableInterrupts);
}


/*******************************************************************************
* Function Name: ServoPwm3_TriggerCommand
********************************************************************************
*
* Summary:
*  Triggers the designated command to occur on the designated TCPWM instances.
*  The mask can be used to apply this command simultaneously to more than one
*  instance.  This allows multiple TCPWM instances to be synchronized.
*
* Parameters:
*  mask: A combination of mask bits for each instance of the TCPWM that the
*        command should apply to.  This function from one instance can be used
*        to apply the command to any of the instances in the design.
*        The mask value for a specific instance is available with the MASK
*        define.
*  command: Enumerated command values. Capture command only applicable for
*           Timer/Counter with Capture and PWM modes.
*   Values:
*     - ServoPwm3_CMD_CAPTURE    - Trigger Capture/Switch command
*     - ServoPwm3_CMD_RELOAD     - Trigger Reload/Index command
*     - ServoPwm3_CMD_STOP       - Trigger Stop/Kill command
*     - ServoPwm3_CMD_START      - Trigger Start/phiB command
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_TriggerCommand(uint32 mask, uint32 command)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();

    ServoPwm3_COMMAND_REG = ((uint32)(mask << command));

    CyExitCriticalSection(enableInterrupts);
}


/*******************************************************************************
* Function Name: ServoPwm3_ReadStatus
********************************************************************************
*
* Summary:
*  Reads the status of the ServoPwm3.
*
* Parameters:
*  None
*
* Return:
*  Status
*   Values:
*     - ServoPwm3_STATUS_DOWN    - Set if counting down
*     - ServoPwm3_STATUS_RUNNING - Set if counter is running
*
*******************************************************************************/
uint32 ServoPwm3_ReadStatus(void)
{
    return ((ServoPwm3_STATUS_REG >> ServoPwm3_RUNNING_STATUS_SHIFT) |
            (ServoPwm3_STATUS_REG & ServoPwm3_STATUS_DOWN));
}


/*******************************************************************************
* Function Name: ServoPwm3_SetInterruptMode
********************************************************************************
*
* Summary:
*  Sets the interrupt mask to control which interrupt
*  requests generate the interrupt signal.
*
* Parameters:
*   interruptMask: Mask of bits to be enabled
*   Values:
*     - ServoPwm3_INTR_MASK_TC       - Terminal count mask
*     - ServoPwm3_INTR_MASK_CC_MATCH - Compare count / capture mask
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_SetInterruptMode(uint32 interruptMask)
{
    ServoPwm3_INTERRUPT_MASK_REG =  interruptMask;
}


/*******************************************************************************
* Function Name: ServoPwm3_GetInterruptSourceMasked
********************************************************************************
*
* Summary:
*  Gets the interrupt requests masked by the interrupt mask.
*
* Parameters:
*   None
*
* Return:
*  Masked interrupt source
*   Values:
*     - ServoPwm3_INTR_MASK_TC       - Terminal count mask
*     - ServoPwm3_INTR_MASK_CC_MATCH - Compare count / capture mask
*
*******************************************************************************/
uint32 ServoPwm3_GetInterruptSourceMasked(void)
{
    return (ServoPwm3_INTERRUPT_MASKED_REG);
}


/*******************************************************************************
* Function Name: ServoPwm3_GetInterruptSource
********************************************************************************
*
* Summary:
*  Gets the interrupt requests (without masking).
*
* Parameters:
*  None
*
* Return:
*  Interrupt request value
*   Values:
*     - ServoPwm3_INTR_MASK_TC       - Terminal count mask
*     - ServoPwm3_INTR_MASK_CC_MATCH - Compare count / capture mask
*
*******************************************************************************/
uint32 ServoPwm3_GetInterruptSource(void)
{
    return (ServoPwm3_INTERRUPT_REQ_REG);
}


/*******************************************************************************
* Function Name: ServoPwm3_ClearInterrupt
********************************************************************************
*
* Summary:
*  Clears the interrupt request.
*
* Parameters:
*   interruptMask: Mask of interrupts to clear
*   Values:
*     - ServoPwm3_INTR_MASK_TC       - Terminal count mask
*     - ServoPwm3_INTR_MASK_CC_MATCH - Compare count / capture mask
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_ClearInterrupt(uint32 interruptMask)
{
    ServoPwm3_INTERRUPT_REQ_REG = interruptMask;
}


/*******************************************************************************
* Function Name: ServoPwm3_SetInterrupt
********************************************************************************
*
* Summary:
*  Sets a software interrupt request.
*
* Parameters:
*   interruptMask: Mask of interrupts to set
*   Values:
*     - ServoPwm3_INTR_MASK_TC       - Terminal count mask
*     - ServoPwm3_INTR_MASK_CC_MATCH - Compare count / capture mask
*
* Return:
*  None
*
*******************************************************************************/
void ServoPwm3_SetInterrupt(uint32 interruptMask)
{
    ServoPwm3_INTERRUPT_SET_REG = interruptMask;
}


/* [] END OF FILE */
