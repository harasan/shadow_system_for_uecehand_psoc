/*******************************************************************************
* File Name: AGNDOut.c  
* Version 2.20
*
* Description:
*  This file contains APIs to set up the Pins component for low power modes.
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "AGNDOut.h"

static AGNDOut_BACKUP_STRUCT  AGNDOut_backup = {0u, 0u, 0u};


/*******************************************************************************
* Function Name: AGNDOut_Sleep
****************************************************************************//**
*
* \brief Stores the pin configuration and prepares the pin for entering chip 
*  deep-sleep/hibernate modes. This function applies only to SIO and USBIO pins.
*  It should not be called for GPIO or GPIO_OVT pins.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None 
*  
* \sideeffect
*  For SIO pins, this function configures the pin input threshold to CMOS and
*  drive level to Vddio. This is needed for SIO pins when in device 
*  deep-sleep/hibernate modes.
*
* \funcusage
*  \snippet AGNDOut_SUT.c usage_AGNDOut_Sleep_Wakeup
*******************************************************************************/
void AGNDOut_Sleep(void)
{
    #if defined(AGNDOut__PC)
        AGNDOut_backup.pcState = AGNDOut_PC;
    #else
        #if (CY_PSOC4_4200L)
            /* Save the regulator state and put the PHY into suspend mode */
            AGNDOut_backup.usbState = AGNDOut_CR1_REG;
            AGNDOut_USB_POWER_REG |= AGNDOut_USBIO_ENTER_SLEEP;
            AGNDOut_CR1_REG &= AGNDOut_USBIO_CR1_OFF;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(AGNDOut__SIO)
        AGNDOut_backup.sioState = AGNDOut_SIO_REG;
        /* SIO requires unregulated output buffer and single ended input buffer */
        AGNDOut_SIO_REG &= (uint32)(~AGNDOut_SIO_LPM_MASK);
    #endif  
}


/*******************************************************************************
* Function Name: AGNDOut_Wakeup
****************************************************************************//**
*
* \brief Restores the pin configuration that was saved during Pin_Sleep(). This 
* function applies only to SIO and USBIO pins. It should not be called for
* GPIO or GPIO_OVT pins.
*
* For USBIO pins, the wakeup is only triggered for falling edge interrupts.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None
*  
* \funcusage
*  Refer to AGNDOut_Sleep() for an example usage.
*******************************************************************************/
void AGNDOut_Wakeup(void)
{
    #if defined(AGNDOut__PC)
        AGNDOut_PC = AGNDOut_backup.pcState;
    #else
        #if (CY_PSOC4_4200L)
            /* Restore the regulator state and come out of suspend mode */
            AGNDOut_USB_POWER_REG &= AGNDOut_USBIO_EXIT_SLEEP_PH1;
            AGNDOut_CR1_REG = AGNDOut_backup.usbState;
            AGNDOut_USB_POWER_REG &= AGNDOut_USBIO_EXIT_SLEEP_PH2;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(AGNDOut__SIO)
        AGNDOut_SIO_REG = AGNDOut_backup.sioState;
    #endif
}


/* [] END OF FILE */
