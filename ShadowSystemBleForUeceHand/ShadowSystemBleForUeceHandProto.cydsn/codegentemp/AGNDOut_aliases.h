/*******************************************************************************
* File Name: AGNDOut.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_AGNDOut_ALIASES_H) /* Pins AGNDOut_ALIASES_H */
#define CY_PINS_AGNDOut_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define AGNDOut_0			(AGNDOut__0__PC)
#define AGNDOut_0_PS		(AGNDOut__0__PS)
#define AGNDOut_0_PC		(AGNDOut__0__PC)
#define AGNDOut_0_DR		(AGNDOut__0__DR)
#define AGNDOut_0_SHIFT	(AGNDOut__0__SHIFT)
#define AGNDOut_0_INTR	((uint16)((uint16)0x0003u << (AGNDOut__0__SHIFT*2u)))

#define AGNDOut_INTR_ALL	 ((uint16)(AGNDOut_0_INTR))


#endif /* End Pins AGNDOut_ALIASES_H */


/* [] END OF FILE */
