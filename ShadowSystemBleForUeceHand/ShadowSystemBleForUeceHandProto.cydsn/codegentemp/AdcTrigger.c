/*******************************************************************************
* File Name: AdcTrigger.c
* Version 3.30
*
* Description:
*  The PWM User Module consist of an 8 or 16-bit counter with two 8 or 16-bit
*  comparitors. Each instance of this user module is capable of generating
*  two PWM outputs with the same period. The pulse width is selectable between
*  1 and 255/65535. The period is selectable between 2 and 255/65536 clocks.
*  The compare value output may be configured to be active when the present
*  counter is less than or less than/equal to the compare value.
*  A terminal count output is also provided. It generates a pulse one clock
*  width wide when the counter is equal to zero.
*
* Note:
*
*******************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
********************************************************************************/

#include "AdcTrigger.h"

/* Error message for removed <resource> through optimization */
#ifdef AdcTrigger_PWMUDB_genblk1_ctrlreg__REMOVED
    #error PWM_v3_30 detected with a constant 0 for the enable or \
         constant 1 for reset. This will prevent the component from operating.
#endif /* AdcTrigger_PWMUDB_genblk1_ctrlreg__REMOVED */

uint8 AdcTrigger_initVar = 0u;


/*******************************************************************************
* Function Name: AdcTrigger_Start
********************************************************************************
*
* Summary:
*  The start function initializes the pwm with the default values, the
*  enables the counter to begin counting.  It does not enable interrupts,
*  the EnableInt command should be called if interrupt generation is required.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  AdcTrigger_initVar: Is modified when this function is called for the
*   first time. Is used to ensure that initialization happens only once.
*
*******************************************************************************/
void AdcTrigger_Start(void) 
{
    /* If not Initialized then initialize all required hardware and software */
    if(AdcTrigger_initVar == 0u)
    {
        AdcTrigger_Init();
        AdcTrigger_initVar = 1u;
    }
    AdcTrigger_Enable();

}


/*******************************************************************************
* Function Name: AdcTrigger_Init
********************************************************************************
*
* Summary:
*  Initialize component's parameters to the parameters set by user in the
*  customizer of the component placed onto schematic. Usually called in
*  AdcTrigger_Start().
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void AdcTrigger_Init(void) 
{
    #if (AdcTrigger_UsingFixedFunction || AdcTrigger_UseControl)
        uint8 ctrl;
    #endif /* (AdcTrigger_UsingFixedFunction || AdcTrigger_UseControl) */

    #if(!AdcTrigger_UsingFixedFunction)
        #if(AdcTrigger_UseStatus)
            /* Interrupt State Backup for Critical Region*/
            uint8 AdcTrigger_interruptState;
        #endif /* (AdcTrigger_UseStatus) */
    #endif /* (!AdcTrigger_UsingFixedFunction) */

    #if (AdcTrigger_UsingFixedFunction)
        /* You are allowed to write the compare value (FF only) */
        AdcTrigger_CONTROL |= AdcTrigger_CFG0_MODE;
        #if (AdcTrigger_DeadBand2_4)
            AdcTrigger_CONTROL |= AdcTrigger_CFG0_DB;
        #endif /* (AdcTrigger_DeadBand2_4) */

        ctrl = AdcTrigger_CONTROL3 & ((uint8 )(~AdcTrigger_CTRL_CMPMODE1_MASK));
        AdcTrigger_CONTROL3 = ctrl | AdcTrigger_DEFAULT_COMPARE1_MODE;

         /* Clear and Set SYNCTC and SYNCCMP bits of RT1 register */
        AdcTrigger_RT1 &= ((uint8)(~AdcTrigger_RT1_MASK));
        AdcTrigger_RT1 |= AdcTrigger_SYNC;

        /*Enable DSI Sync all all inputs of the PWM*/
        AdcTrigger_RT1 &= ((uint8)(~AdcTrigger_SYNCDSI_MASK));
        AdcTrigger_RT1 |= AdcTrigger_SYNCDSI_EN;

    #elif (AdcTrigger_UseControl)
        /* Set the default compare mode defined in the parameter */
        ctrl = AdcTrigger_CONTROL & ((uint8)(~AdcTrigger_CTRL_CMPMODE2_MASK)) &
                ((uint8)(~AdcTrigger_CTRL_CMPMODE1_MASK));
        AdcTrigger_CONTROL = ctrl | AdcTrigger_DEFAULT_COMPARE2_MODE |
                                   AdcTrigger_DEFAULT_COMPARE1_MODE;
    #endif /* (AdcTrigger_UsingFixedFunction) */

    #if (!AdcTrigger_UsingFixedFunction)
        #if (AdcTrigger_Resolution == 8)
            /* Set FIFO 0 to 1 byte register for period*/
            AdcTrigger_AUX_CONTROLDP0 |= (AdcTrigger_AUX_CTRL_FIFO0_CLR);
        #else /* (AdcTrigger_Resolution == 16)*/
            /* Set FIFO 0 to 1 byte register for period */
            AdcTrigger_AUX_CONTROLDP0 |= (AdcTrigger_AUX_CTRL_FIFO0_CLR);
            AdcTrigger_AUX_CONTROLDP1 |= (AdcTrigger_AUX_CTRL_FIFO0_CLR);
        #endif /* (AdcTrigger_Resolution == 8) */

        AdcTrigger_WriteCounter(AdcTrigger_INIT_PERIOD_VALUE);
    #endif /* (!AdcTrigger_UsingFixedFunction) */

    AdcTrigger_WritePeriod(AdcTrigger_INIT_PERIOD_VALUE);

        #if (AdcTrigger_UseOneCompareMode)
            AdcTrigger_WriteCompare(AdcTrigger_INIT_COMPARE_VALUE1);
        #else
            AdcTrigger_WriteCompare1(AdcTrigger_INIT_COMPARE_VALUE1);
            AdcTrigger_WriteCompare2(AdcTrigger_INIT_COMPARE_VALUE2);
        #endif /* (AdcTrigger_UseOneCompareMode) */

        #if (AdcTrigger_KillModeMinTime)
            AdcTrigger_WriteKillTime(AdcTrigger_MinimumKillTime);
        #endif /* (AdcTrigger_KillModeMinTime) */

        #if (AdcTrigger_DeadBandUsed)
            AdcTrigger_WriteDeadTime(AdcTrigger_INIT_DEAD_TIME);
        #endif /* (AdcTrigger_DeadBandUsed) */

    #if (AdcTrigger_UseStatus || AdcTrigger_UsingFixedFunction)
        AdcTrigger_SetInterruptMode(AdcTrigger_INIT_INTERRUPTS_MODE);
    #endif /* (AdcTrigger_UseStatus || AdcTrigger_UsingFixedFunction) */

    #if (AdcTrigger_UsingFixedFunction)
        /* Globally Enable the Fixed Function Block chosen */
        AdcTrigger_GLOBAL_ENABLE |= AdcTrigger_BLOCK_EN_MASK;
        /* Set the Interrupt source to come from the status register */
        AdcTrigger_CONTROL2 |= AdcTrigger_CTRL2_IRQ_SEL;
    #else
        #if(AdcTrigger_UseStatus)

            /* CyEnterCriticalRegion and CyExitCriticalRegion are used to mark following region critical*/
            /* Enter Critical Region*/
            AdcTrigger_interruptState = CyEnterCriticalSection();
            /* Use the interrupt output of the status register for IRQ output */
            AdcTrigger_STATUS_AUX_CTRL |= AdcTrigger_STATUS_ACTL_INT_EN_MASK;

             /* Exit Critical Region*/
            CyExitCriticalSection(AdcTrigger_interruptState);

            /* Clear the FIFO to enable the AdcTrigger_STATUS_FIFOFULL
                   bit to be set on FIFO full. */
            AdcTrigger_ClearFIFO();
        #endif /* (AdcTrigger_UseStatus) */
    #endif /* (AdcTrigger_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: AdcTrigger_Enable
********************************************************************************
*
* Summary:
*  Enables the PWM block operation
*
* Parameters:
*  None
*
* Return:
*  None
*
* Side Effects:
*  This works only if software enable mode is chosen
*
*******************************************************************************/
void AdcTrigger_Enable(void) 
{
    /* Globally Enable the Fixed Function Block chosen */
    #if (AdcTrigger_UsingFixedFunction)
        AdcTrigger_GLOBAL_ENABLE |= AdcTrigger_BLOCK_EN_MASK;
        AdcTrigger_GLOBAL_STBY_ENABLE |= AdcTrigger_BLOCK_STBY_EN_MASK;
    #endif /* (AdcTrigger_UsingFixedFunction) */

    /* Enable the PWM from the control register  */
    #if (AdcTrigger_UseControl || AdcTrigger_UsingFixedFunction)
        AdcTrigger_CONTROL |= AdcTrigger_CTRL_ENABLE;
    #endif /* (AdcTrigger_UseControl || AdcTrigger_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: AdcTrigger_Stop
********************************************************************************
*
* Summary:
*  The stop function halts the PWM, but does not change any modes or disable
*  interrupts.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Side Effects:
*  If the Enable mode is set to Hardware only then this function
*  has no effect on the operation of the PWM
*
*******************************************************************************/
void AdcTrigger_Stop(void) 
{
    #if (AdcTrigger_UseControl || AdcTrigger_UsingFixedFunction)
        AdcTrigger_CONTROL &= ((uint8)(~AdcTrigger_CTRL_ENABLE));
    #endif /* (AdcTrigger_UseControl || AdcTrigger_UsingFixedFunction) */

    /* Globally disable the Fixed Function Block chosen */
    #if (AdcTrigger_UsingFixedFunction)
        AdcTrigger_GLOBAL_ENABLE &= ((uint8)(~AdcTrigger_BLOCK_EN_MASK));
        AdcTrigger_GLOBAL_STBY_ENABLE &= ((uint8)(~AdcTrigger_BLOCK_STBY_EN_MASK));
    #endif /* (AdcTrigger_UsingFixedFunction) */
}

#if (AdcTrigger_UseOneCompareMode)
    #if (AdcTrigger_CompareMode1SW)


        /*******************************************************************************
        * Function Name: AdcTrigger_SetCompareMode
        ********************************************************************************
        *
        * Summary:
        *  This function writes the Compare Mode for the pwm output when in Dither mode,
        *  Center Align Mode or One Output Mode.
        *
        * Parameters:
        *  comparemode:  The new compare mode for the PWM output. Use the compare types
        *                defined in the H file as input arguments.
        *
        * Return:
        *  None
        *
        *******************************************************************************/
        void AdcTrigger_SetCompareMode(uint8 comparemode) 
        {
            #if(AdcTrigger_UsingFixedFunction)

                #if(0 != AdcTrigger_CTRL_CMPMODE1_SHIFT)
                    uint8 comparemodemasked = ((uint8)((uint8)comparemode << AdcTrigger_CTRL_CMPMODE1_SHIFT));
                #else
                    uint8 comparemodemasked = comparemode;
                #endif /* (0 != AdcTrigger_CTRL_CMPMODE1_SHIFT) */

                AdcTrigger_CONTROL3 &= ((uint8)(~AdcTrigger_CTRL_CMPMODE1_MASK)); /*Clear Existing Data */
                AdcTrigger_CONTROL3 |= comparemodemasked;

            #elif (AdcTrigger_UseControl)

                #if(0 != AdcTrigger_CTRL_CMPMODE1_SHIFT)
                    uint8 comparemode1masked = ((uint8)((uint8)comparemode << AdcTrigger_CTRL_CMPMODE1_SHIFT)) &
                                                AdcTrigger_CTRL_CMPMODE1_MASK;
                #else
                    uint8 comparemode1masked = comparemode & AdcTrigger_CTRL_CMPMODE1_MASK;
                #endif /* (0 != AdcTrigger_CTRL_CMPMODE1_SHIFT) */

                #if(0 != AdcTrigger_CTRL_CMPMODE2_SHIFT)
                    uint8 comparemode2masked = ((uint8)((uint8)comparemode << AdcTrigger_CTRL_CMPMODE2_SHIFT)) &
                                               AdcTrigger_CTRL_CMPMODE2_MASK;
                #else
                    uint8 comparemode2masked = comparemode & AdcTrigger_CTRL_CMPMODE2_MASK;
                #endif /* (0 != AdcTrigger_CTRL_CMPMODE2_SHIFT) */

                /*Clear existing mode */
                AdcTrigger_CONTROL &= ((uint8)(~(AdcTrigger_CTRL_CMPMODE1_MASK |
                                            AdcTrigger_CTRL_CMPMODE2_MASK)));
                AdcTrigger_CONTROL |= (comparemode1masked | comparemode2masked);

            #else
                uint8 temp = comparemode;
            #endif /* (AdcTrigger_UsingFixedFunction) */
        }
    #endif /* AdcTrigger_CompareMode1SW */

#else /* UseOneCompareMode */

    #if (AdcTrigger_CompareMode1SW)


        /*******************************************************************************
        * Function Name: AdcTrigger_SetCompareMode1
        ********************************************************************************
        *
        * Summary:
        *  This function writes the Compare Mode for the pwm or pwm1 output
        *
        * Parameters:
        *  comparemode:  The new compare mode for the PWM output. Use the compare types
        *                defined in the H file as input arguments.
        *
        * Return:
        *  None
        *
        *******************************************************************************/
        void AdcTrigger_SetCompareMode1(uint8 comparemode) 
        {
            #if(0 != AdcTrigger_CTRL_CMPMODE1_SHIFT)
                uint8 comparemodemasked = ((uint8)((uint8)comparemode << AdcTrigger_CTRL_CMPMODE1_SHIFT)) &
                                           AdcTrigger_CTRL_CMPMODE1_MASK;
            #else
                uint8 comparemodemasked = comparemode & AdcTrigger_CTRL_CMPMODE1_MASK;
            #endif /* (0 != AdcTrigger_CTRL_CMPMODE1_SHIFT) */

            #if (AdcTrigger_UseControl)
                AdcTrigger_CONTROL &= ((uint8)(~AdcTrigger_CTRL_CMPMODE1_MASK)); /*Clear existing mode */
                AdcTrigger_CONTROL |= comparemodemasked;
            #endif /* (AdcTrigger_UseControl) */
        }
    #endif /* AdcTrigger_CompareMode1SW */

#if (AdcTrigger_CompareMode2SW)


    /*******************************************************************************
    * Function Name: AdcTrigger_SetCompareMode2
    ********************************************************************************
    *
    * Summary:
    *  This function writes the Compare Mode for the pwm or pwm2 output
    *
    * Parameters:
    *  comparemode:  The new compare mode for the PWM output. Use the compare types
    *                defined in the H file as input arguments.
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void AdcTrigger_SetCompareMode2(uint8 comparemode) 
    {

        #if(0 != AdcTrigger_CTRL_CMPMODE2_SHIFT)
            uint8 comparemodemasked = ((uint8)((uint8)comparemode << AdcTrigger_CTRL_CMPMODE2_SHIFT)) &
                                                 AdcTrigger_CTRL_CMPMODE2_MASK;
        #else
            uint8 comparemodemasked = comparemode & AdcTrigger_CTRL_CMPMODE2_MASK;
        #endif /* (0 != AdcTrigger_CTRL_CMPMODE2_SHIFT) */

        #if (AdcTrigger_UseControl)
            AdcTrigger_CONTROL &= ((uint8)(~AdcTrigger_CTRL_CMPMODE2_MASK)); /*Clear existing mode */
            AdcTrigger_CONTROL |= comparemodemasked;
        #endif /* (AdcTrigger_UseControl) */
    }
    #endif /*AdcTrigger_CompareMode2SW */

#endif /* UseOneCompareMode */


#if (!AdcTrigger_UsingFixedFunction)


    /*******************************************************************************
    * Function Name: AdcTrigger_WriteCounter
    ********************************************************************************
    *
    * Summary:
    *  Writes a new counter value directly to the counter register. This will be
    *  implemented for that currently running period and only that period. This API
    *  is valid only for UDB implementation and not available for fixed function
    *  PWM implementation.
    *
    * Parameters:
    *  counter:  The period new period counter value.
    *
    * Return:
    *  None
    *
    * Side Effects:
    *  The PWM Period will be reloaded when a counter value will be a zero
    *
    *******************************************************************************/
    void AdcTrigger_WriteCounter(uint8 counter) \
                                       
    {
        CY_SET_REG8(AdcTrigger_COUNTER_LSB_PTR, counter);
    }


    /*******************************************************************************
    * Function Name: AdcTrigger_ReadCounter
    ********************************************************************************
    *
    * Summary:
    *  This function returns the current value of the counter.  It doesn't matter
    *  if the counter is enabled or running.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  The current value of the counter.
    *
    *******************************************************************************/
    uint8 AdcTrigger_ReadCounter(void) 
    {
        /* Force capture by reading Accumulator */
        /* Must first do a software capture to be able to read the counter */
        /* It is up to the user code to make sure there isn't already captured data in the FIFO */
          (void)CY_GET_REG8(AdcTrigger_COUNTERCAP_LSB_PTR_8BIT);

        /* Read the data from the FIFO */
        return (CY_GET_REG8(AdcTrigger_CAPTURE_LSB_PTR));
    }

    #if (AdcTrigger_UseStatus)


        /*******************************************************************************
        * Function Name: AdcTrigger_ClearFIFO
        ********************************************************************************
        *
        * Summary:
        *  This function clears all capture data from the capture FIFO
        *
        * Parameters:
        *  None
        *
        * Return:
        *  None
        *
        *******************************************************************************/
        void AdcTrigger_ClearFIFO(void) 
        {
            while(0u != (AdcTrigger_ReadStatusRegister() & AdcTrigger_STATUS_FIFONEMPTY))
            {
                (void)AdcTrigger_ReadCapture();
            }
        }

    #endif /* AdcTrigger_UseStatus */

#endif /* !AdcTrigger_UsingFixedFunction */


/*******************************************************************************
* Function Name: AdcTrigger_WritePeriod
********************************************************************************
*
* Summary:
*  This function is used to change the period of the counter.  The new period
*  will be loaded the next time terminal count is detected.
*
* Parameters:
*  period:  Period value. May be between 1 and (2^Resolution)-1.  A value of 0
*           will result in the counter remaining at zero.
*
* Return:
*  None
*
*******************************************************************************/
void AdcTrigger_WritePeriod(uint8 period) 
{
    #if(AdcTrigger_UsingFixedFunction)
        CY_SET_REG16(AdcTrigger_PERIOD_LSB_PTR, (uint16)period);
    #else
        CY_SET_REG8(AdcTrigger_PERIOD_LSB_PTR, period);
    #endif /* (AdcTrigger_UsingFixedFunction) */
}

#if (AdcTrigger_UseOneCompareMode)


    /*******************************************************************************
    * Function Name: AdcTrigger_WriteCompare
    ********************************************************************************
    *
    * Summary:
    *  This funtion is used to change the compare1 value when the PWM is in Dither
    *  mode. The compare output will reflect the new value on the next UDB clock.
    *  The compare output will be driven high when the present counter value is
    *  compared to the compare value based on the compare mode defined in
    *  Dither Mode.
    *
    * Parameters:
    *  compare:  New compare value.
    *
    * Return:
    *  None
    *
    * Side Effects:
    *  This function is only available if the PWM mode parameter is set to
    *  Dither Mode, Center Aligned Mode or One Output Mode
    *
    *******************************************************************************/
    void AdcTrigger_WriteCompare(uint8 compare) \
                                       
    {
        #if(AdcTrigger_UsingFixedFunction)
            CY_SET_REG16(AdcTrigger_COMPARE1_LSB_PTR, (uint16)compare);
        #else
            CY_SET_REG8(AdcTrigger_COMPARE1_LSB_PTR, compare);
        #endif /* (AdcTrigger_UsingFixedFunction) */

        #if (AdcTrigger_PWMMode == AdcTrigger__B_PWM__DITHER)
            #if(AdcTrigger_UsingFixedFunction)
                CY_SET_REG16(AdcTrigger_COMPARE2_LSB_PTR, (uint16)(compare + 1u));
            #else
                CY_SET_REG8(AdcTrigger_COMPARE2_LSB_PTR, (compare + 1u));
            #endif /* (AdcTrigger_UsingFixedFunction) */
        #endif /* (AdcTrigger_PWMMode == AdcTrigger__B_PWM__DITHER) */
    }


#else


    /*******************************************************************************
    * Function Name: AdcTrigger_WriteCompare1
    ********************************************************************************
    *
    * Summary:
    *  This funtion is used to change the compare1 value.  The compare output will
    *  reflect the new value on the next UDB clock.  The compare output will be
    *  driven high when the present counter value is less than or less than or
    *  equal to the compare register, depending on the mode.
    *
    * Parameters:
    *  compare:  New compare value.
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void AdcTrigger_WriteCompare1(uint8 compare) \
                                        
    {
        #if(AdcTrigger_UsingFixedFunction)
            CY_SET_REG16(AdcTrigger_COMPARE1_LSB_PTR, (uint16)compare);
        #else
            CY_SET_REG8(AdcTrigger_COMPARE1_LSB_PTR, compare);
        #endif /* (AdcTrigger_UsingFixedFunction) */
    }


    /*******************************************************************************
    * Function Name: AdcTrigger_WriteCompare2
    ********************************************************************************
    *
    * Summary:
    *  This funtion is used to change the compare value, for compare1 output.
    *  The compare output will reflect the new value on the next UDB clock.
    *  The compare output will be driven high when the present counter value is
    *  less than or less than or equal to the compare register, depending on the
    *  mode.
    *
    * Parameters:
    *  compare:  New compare value.
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void AdcTrigger_WriteCompare2(uint8 compare) \
                                        
    {
        #if(AdcTrigger_UsingFixedFunction)
            CY_SET_REG16(AdcTrigger_COMPARE2_LSB_PTR, compare);
        #else
            CY_SET_REG8(AdcTrigger_COMPARE2_LSB_PTR, compare);
        #endif /* (AdcTrigger_UsingFixedFunction) */
    }
#endif /* UseOneCompareMode */

#if (AdcTrigger_DeadBandUsed)


    /*******************************************************************************
    * Function Name: AdcTrigger_WriteDeadTime
    ********************************************************************************
    *
    * Summary:
    *  This function writes the dead-band counts to the corresponding register
    *
    * Parameters:
    *  deadtime:  Number of counts for dead time
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void AdcTrigger_WriteDeadTime(uint8 deadtime) 
    {
        /* If using the Dead Band 1-255 mode then just write the register */
        #if(!AdcTrigger_DeadBand2_4)
            CY_SET_REG8(AdcTrigger_DEADBAND_COUNT_PTR, deadtime);
        #else
            /* Otherwise the data has to be masked and offset */
            /* Clear existing data */
            AdcTrigger_DEADBAND_COUNT &= ((uint8)(~AdcTrigger_DEADBAND_COUNT_MASK));

            /* Set new dead time */
            #if(AdcTrigger_DEADBAND_COUNT_SHIFT)
                AdcTrigger_DEADBAND_COUNT |= ((uint8)((uint8)deadtime << AdcTrigger_DEADBAND_COUNT_SHIFT)) &
                                                    AdcTrigger_DEADBAND_COUNT_MASK;
            #else
                AdcTrigger_DEADBAND_COUNT |= deadtime & AdcTrigger_DEADBAND_COUNT_MASK;
            #endif /* (AdcTrigger_DEADBAND_COUNT_SHIFT) */

        #endif /* (!AdcTrigger_DeadBand2_4) */
    }


    /*******************************************************************************
    * Function Name: AdcTrigger_ReadDeadTime
    ********************************************************************************
    *
    * Summary:
    *  This function reads the dead-band counts from the corresponding register
    *
    * Parameters:
    *  None
    *
    * Return:
    *  Dead Band Counts
    *
    *******************************************************************************/
    uint8 AdcTrigger_ReadDeadTime(void) 
    {
        /* If using the Dead Band 1-255 mode then just read the register */
        #if(!AdcTrigger_DeadBand2_4)
            return (CY_GET_REG8(AdcTrigger_DEADBAND_COUNT_PTR));
        #else

            /* Otherwise the data has to be masked and offset */
            #if(AdcTrigger_DEADBAND_COUNT_SHIFT)
                return ((uint8)(((uint8)(AdcTrigger_DEADBAND_COUNT & AdcTrigger_DEADBAND_COUNT_MASK)) >>
                                                                           AdcTrigger_DEADBAND_COUNT_SHIFT));
            #else
                return (AdcTrigger_DEADBAND_COUNT & AdcTrigger_DEADBAND_COUNT_MASK);
            #endif /* (AdcTrigger_DEADBAND_COUNT_SHIFT) */
        #endif /* (!AdcTrigger_DeadBand2_4) */
    }
#endif /* DeadBandUsed */

#if (AdcTrigger_UseStatus || AdcTrigger_UsingFixedFunction)


    /*******************************************************************************
    * Function Name: AdcTrigger_SetInterruptMode
    ********************************************************************************
    *
    * Summary:
    *  This function configures the interrupts mask control of theinterrupt
    *  source status register.
    *
    * Parameters:
    *  uint8 interruptMode: Bit field containing the interrupt sources enabled
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void AdcTrigger_SetInterruptMode(uint8 interruptMode) 
    {
        CY_SET_REG8(AdcTrigger_STATUS_MASK_PTR, interruptMode);
    }


    /*******************************************************************************
    * Function Name: AdcTrigger_ReadStatusRegister
    ********************************************************************************
    *
    * Summary:
    *  This function returns the current state of the status register.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  uint8 : Current status register value. The status register bits are:
    *  [7:6] : Unused(0)
    *  [5]   : Kill event output
    *  [4]   : FIFO not empty
    *  [3]   : FIFO full
    *  [2]   : Terminal count
    *  [1]   : Compare output 2
    *  [0]   : Compare output 1
    *
    *******************************************************************************/
    uint8 AdcTrigger_ReadStatusRegister(void) 
    {
        return (CY_GET_REG8(AdcTrigger_STATUS_PTR));
    }

#endif /* (AdcTrigger_UseStatus || AdcTrigger_UsingFixedFunction) */


#if (AdcTrigger_UseControl)


    /*******************************************************************************
    * Function Name: AdcTrigger_ReadControlRegister
    ********************************************************************************
    *
    * Summary:
    *  Returns the current state of the control register. This API is available
    *  only if the control register is not removed.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  uint8 : Current control register value
    *
    *******************************************************************************/
    uint8 AdcTrigger_ReadControlRegister(void) 
    {
        uint8 result;

        result = CY_GET_REG8(AdcTrigger_CONTROL_PTR);
        return (result);
    }


    /*******************************************************************************
    * Function Name: AdcTrigger_WriteControlRegister
    ********************************************************************************
    *
    * Summary:
    *  Sets the bit field of the control register. This API is available only if
    *  the control register is not removed.
    *
    * Parameters:
    *  uint8 control: Control register bit field, The status register bits are:
    *  [7]   : PWM Enable
    *  [6]   : Reset
    *  [5:3] : Compare Mode2
    *  [2:0] : Compare Mode2
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void AdcTrigger_WriteControlRegister(uint8 control) 
    {
        CY_SET_REG8(AdcTrigger_CONTROL_PTR, control);
    }

#endif /* (AdcTrigger_UseControl) */


#if (!AdcTrigger_UsingFixedFunction)


    /*******************************************************************************
    * Function Name: AdcTrigger_ReadCapture
    ********************************************************************************
    *
    * Summary:
    *  Reads the capture value from the capture FIFO.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  uint8/uint16: The current capture value
    *
    *******************************************************************************/
    uint8 AdcTrigger_ReadCapture(void) 
    {
        return (CY_GET_REG8(AdcTrigger_CAPTURE_LSB_PTR));
    }

#endif /* (!AdcTrigger_UsingFixedFunction) */


#if (AdcTrigger_UseOneCompareMode)


    /*******************************************************************************
    * Function Name: AdcTrigger_ReadCompare
    ********************************************************************************
    *
    * Summary:
    *  Reads the compare value for the compare output when the PWM Mode parameter is
    *  set to Dither mode, Center Aligned mode, or One Output mode.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  uint8/uint16: Current compare value
    *
    *******************************************************************************/
    uint8 AdcTrigger_ReadCompare(void) 
    {
        #if(AdcTrigger_UsingFixedFunction)
            return ((uint8)CY_GET_REG16(AdcTrigger_COMPARE1_LSB_PTR));
        #else
            return (CY_GET_REG8(AdcTrigger_COMPARE1_LSB_PTR));
        #endif /* (AdcTrigger_UsingFixedFunction) */
    }

#else


    /*******************************************************************************
    * Function Name: AdcTrigger_ReadCompare1
    ********************************************************************************
    *
    * Summary:
    *  Reads the compare value for the compare1 output.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  uint8/uint16: Current compare value.
    *
    *******************************************************************************/
    uint8 AdcTrigger_ReadCompare1(void) 
    {
        return (CY_GET_REG8(AdcTrigger_COMPARE1_LSB_PTR));
    }


    /*******************************************************************************
    * Function Name: AdcTrigger_ReadCompare2
    ********************************************************************************
    *
    * Summary:
    *  Reads the compare value for the compare2 output.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  uint8/uint16: Current compare value.
    *
    *******************************************************************************/
    uint8 AdcTrigger_ReadCompare2(void) 
    {
        return (CY_GET_REG8(AdcTrigger_COMPARE2_LSB_PTR));
    }

#endif /* (AdcTrigger_UseOneCompareMode) */


/*******************************************************************************
* Function Name: AdcTrigger_ReadPeriod
********************************************************************************
*
* Summary:
*  Reads the period value used by the PWM hardware.
*
* Parameters:
*  None
*
* Return:
*  uint8/16: Period value
*
*******************************************************************************/
uint8 AdcTrigger_ReadPeriod(void) 
{
    #if(AdcTrigger_UsingFixedFunction)
        return ((uint8)CY_GET_REG16(AdcTrigger_PERIOD_LSB_PTR));
    #else
        return (CY_GET_REG8(AdcTrigger_PERIOD_LSB_PTR));
    #endif /* (AdcTrigger_UsingFixedFunction) */
}

#if ( AdcTrigger_KillModeMinTime)


    /*******************************************************************************
    * Function Name: AdcTrigger_WriteKillTime
    ********************************************************************************
    *
    * Summary:
    *  Writes the kill time value used by the hardware when the Kill Mode
    *  is set to Minimum Time.
    *
    * Parameters:
    *  uint8: Minimum Time kill counts
    *
    * Return:
    *  None
    *
    *******************************************************************************/
    void AdcTrigger_WriteKillTime(uint8 killtime) 
    {
        CY_SET_REG8(AdcTrigger_KILLMODEMINTIME_PTR, killtime);
    }


    /*******************************************************************************
    * Function Name: AdcTrigger_ReadKillTime
    ********************************************************************************
    *
    * Summary:
    *  Reads the kill time value used by the hardware when the Kill Mode is set
    *  to Minimum Time.
    *
    * Parameters:
    *  None
    *
    * Return:
    *  uint8: The current Minimum Time kill counts
    *
    *******************************************************************************/
    uint8 AdcTrigger_ReadKillTime(void) 
    {
        return (CY_GET_REG8(AdcTrigger_KILLMODEMINTIME_PTR));
    }

#endif /* ( AdcTrigger_KillModeMinTime) */

/* [] END OF FILE */
