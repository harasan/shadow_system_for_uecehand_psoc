/*******************************************************************************
* File Name: AGNDIn.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_AGNDIn_H) /* Pins AGNDIn_H */
#define CY_PINS_AGNDIn_H

#include "cytypes.h"
#include "cyfitter.h"
#include "AGNDIn_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} AGNDIn_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   AGNDIn_Read(void);
void    AGNDIn_Write(uint8 value);
uint8   AGNDIn_ReadDataReg(void);
#if defined(AGNDIn__PC) || (CY_PSOC4_4200L) 
    void    AGNDIn_SetDriveMode(uint8 mode);
#endif
void    AGNDIn_SetInterruptMode(uint16 position, uint16 mode);
uint8   AGNDIn_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void AGNDIn_Sleep(void); 
void AGNDIn_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(AGNDIn__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define AGNDIn_DRIVE_MODE_BITS        (3)
    #define AGNDIn_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - AGNDIn_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the AGNDIn_SetDriveMode() function.
         *  @{
         */
        #define AGNDIn_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define AGNDIn_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define AGNDIn_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define AGNDIn_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define AGNDIn_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define AGNDIn_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define AGNDIn_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define AGNDIn_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define AGNDIn_MASK               AGNDIn__MASK
#define AGNDIn_SHIFT              AGNDIn__SHIFT
#define AGNDIn_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in AGNDIn_SetInterruptMode() function.
     *  @{
     */
        #define AGNDIn_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define AGNDIn_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define AGNDIn_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define AGNDIn_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(AGNDIn__SIO)
    #define AGNDIn_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(AGNDIn__PC) && (CY_PSOC4_4200L)
    #define AGNDIn_USBIO_ENABLE               ((uint32)0x80000000u)
    #define AGNDIn_USBIO_DISABLE              ((uint32)(~AGNDIn_USBIO_ENABLE))
    #define AGNDIn_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define AGNDIn_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define AGNDIn_USBIO_ENTER_SLEEP          ((uint32)((1u << AGNDIn_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << AGNDIn_USBIO_SUSPEND_DEL_SHIFT)))
    #define AGNDIn_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << AGNDIn_USBIO_SUSPEND_SHIFT)))
    #define AGNDIn_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << AGNDIn_USBIO_SUSPEND_DEL_SHIFT)))
    #define AGNDIn_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(AGNDIn__PC)
    /* Port Configuration */
    #define AGNDIn_PC                 (* (reg32 *) AGNDIn__PC)
#endif
/* Pin State */
#define AGNDIn_PS                     (* (reg32 *) AGNDIn__PS)
/* Data Register */
#define AGNDIn_DR                     (* (reg32 *) AGNDIn__DR)
/* Input Buffer Disable Override */
#define AGNDIn_INP_DIS                (* (reg32 *) AGNDIn__PC2)

/* Interrupt configuration Registers */
#define AGNDIn_INTCFG                 (* (reg32 *) AGNDIn__INTCFG)
#define AGNDIn_INTSTAT                (* (reg32 *) AGNDIn__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define AGNDIn_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(AGNDIn__SIO)
    #define AGNDIn_SIO_REG            (* (reg32 *) AGNDIn__SIO)
#endif /* (AGNDIn__SIO_CFG) */

/* USBIO registers */
#if !defined(AGNDIn__PC) && (CY_PSOC4_4200L)
    #define AGNDIn_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define AGNDIn_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define AGNDIn_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define AGNDIn_DRIVE_MODE_SHIFT       (0x00u)
#define AGNDIn_DRIVE_MODE_MASK        (0x07u << AGNDIn_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins AGNDIn_H */


/* [] END OF FILE */
