/*******************************************************************************
* File Name: OpIn2.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_OpIn2_ALIASES_H) /* Pins OpIn2_ALIASES_H */
#define CY_PINS_OpIn2_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define OpIn2_0			(OpIn2__0__PC)
#define OpIn2_0_PS		(OpIn2__0__PS)
#define OpIn2_0_PC		(OpIn2__0__PC)
#define OpIn2_0_DR		(OpIn2__0__DR)
#define OpIn2_0_SHIFT	(OpIn2__0__SHIFT)
#define OpIn2_0_INTR	((uint16)((uint16)0x0003u << (OpIn2__0__SHIFT*2u)))

#define OpIn2_INTR_ALL	 ((uint16)(OpIn2_0_INTR))


#endif /* End Pins OpIn2_ALIASES_H */


/* [] END OF FILE */
