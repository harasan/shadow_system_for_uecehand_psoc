/*******************************************************************************
* File Name: AdcTrigger_PM.c
* Version 3.30
*
* Description:
*  This file provides the power management source code to API for the
*  PWM.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "AdcTrigger.h"

static AdcTrigger_backupStruct AdcTrigger_backup;


/*******************************************************************************
* Function Name: AdcTrigger_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration of the component.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  AdcTrigger_backup:  Variables of this global structure are modified to
*  store the values of non retention configuration registers when Sleep() API is
*  called.
*
*******************************************************************************/
void AdcTrigger_SaveConfig(void) 
{

    #if(!AdcTrigger_UsingFixedFunction)
        #if(!AdcTrigger_PWMModeIsCenterAligned)
            AdcTrigger_backup.PWMPeriod = AdcTrigger_ReadPeriod();
        #endif /* (!AdcTrigger_PWMModeIsCenterAligned) */
        AdcTrigger_backup.PWMUdb = AdcTrigger_ReadCounter();
        #if (AdcTrigger_UseStatus)
            AdcTrigger_backup.InterruptMaskValue = AdcTrigger_STATUS_MASK;
        #endif /* (AdcTrigger_UseStatus) */

        #if(AdcTrigger_DeadBandMode == AdcTrigger__B_PWM__DBM_256_CLOCKS || \
            AdcTrigger_DeadBandMode == AdcTrigger__B_PWM__DBM_2_4_CLOCKS)
            AdcTrigger_backup.PWMdeadBandValue = AdcTrigger_ReadDeadTime();
        #endif /*  deadband count is either 2-4 clocks or 256 clocks */

        #if(AdcTrigger_KillModeMinTime)
             AdcTrigger_backup.PWMKillCounterPeriod = AdcTrigger_ReadKillTime();
        #endif /* (AdcTrigger_KillModeMinTime) */

        #if(AdcTrigger_UseControl)
            AdcTrigger_backup.PWMControlRegister = AdcTrigger_ReadControlRegister();
        #endif /* (AdcTrigger_UseControl) */
    #endif  /* (!AdcTrigger_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: AdcTrigger_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration of the component.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  AdcTrigger_backup:  Variables of this global structure are used to
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void AdcTrigger_RestoreConfig(void) 
{
        #if(!AdcTrigger_UsingFixedFunction)
            #if(!AdcTrigger_PWMModeIsCenterAligned)
                AdcTrigger_WritePeriod(AdcTrigger_backup.PWMPeriod);
            #endif /* (!AdcTrigger_PWMModeIsCenterAligned) */

            AdcTrigger_WriteCounter(AdcTrigger_backup.PWMUdb);

            #if (AdcTrigger_UseStatus)
                AdcTrigger_STATUS_MASK = AdcTrigger_backup.InterruptMaskValue;
            #endif /* (AdcTrigger_UseStatus) */

            #if(AdcTrigger_DeadBandMode == AdcTrigger__B_PWM__DBM_256_CLOCKS || \
                AdcTrigger_DeadBandMode == AdcTrigger__B_PWM__DBM_2_4_CLOCKS)
                AdcTrigger_WriteDeadTime(AdcTrigger_backup.PWMdeadBandValue);
            #endif /* deadband count is either 2-4 clocks or 256 clocks */

            #if(AdcTrigger_KillModeMinTime)
                AdcTrigger_WriteKillTime(AdcTrigger_backup.PWMKillCounterPeriod);
            #endif /* (AdcTrigger_KillModeMinTime) */

            #if(AdcTrigger_UseControl)
                AdcTrigger_WriteControlRegister(AdcTrigger_backup.PWMControlRegister);
            #endif /* (AdcTrigger_UseControl) */
        #endif  /* (!AdcTrigger_UsingFixedFunction) */
    }


/*******************************************************************************
* Function Name: AdcTrigger_Sleep
********************************************************************************
*
* Summary:
*  Disables block's operation and saves the user configuration. Should be called
*  just prior to entering sleep.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  AdcTrigger_backup.PWMEnableState:  Is modified depending on the enable
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void AdcTrigger_Sleep(void) 
{
    #if(AdcTrigger_UseControl)
        if(AdcTrigger_CTRL_ENABLE == (AdcTrigger_CONTROL & AdcTrigger_CTRL_ENABLE))
        {
            /*Component is enabled */
            AdcTrigger_backup.PWMEnableState = 1u;
        }
        else
        {
            /* Component is disabled */
            AdcTrigger_backup.PWMEnableState = 0u;
        }
    #endif /* (AdcTrigger_UseControl) */

    /* Stop component */
    AdcTrigger_Stop();

    /* Save registers configuration */
    AdcTrigger_SaveConfig();
}


/*******************************************************************************
* Function Name: AdcTrigger_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration. Should be called just after
*  awaking from sleep.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  AdcTrigger_backup.pwmEnable:  Is used to restore the enable state of
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void AdcTrigger_Wakeup(void) 
{
     /* Restore registers values */
    AdcTrigger_RestoreConfig();

    if(AdcTrigger_backup.PWMEnableState != 0u)
    {
        /* Enable component's operation */
        AdcTrigger_Enable();
    } /* Do nothing if component's block was disabled before */

}


/* [] END OF FILE */
