/*******************************************************************************
* File Name: ServoClock.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_ServoClock_H)
#define CY_CLOCK_ServoClock_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
*        Function Prototypes
***************************************/
#if defined CYREG_PERI_DIV_CMD

void ServoClock_StartEx(uint32 alignClkDiv);
#define ServoClock_Start() \
    ServoClock_StartEx(ServoClock__PA_DIV_ID)

#else

void ServoClock_Start(void);

#endif/* CYREG_PERI_DIV_CMD */

void ServoClock_Stop(void);

void ServoClock_SetFractionalDividerRegister(uint16 clkDivider, uint8 clkFractional);

uint16 ServoClock_GetDividerRegister(void);
uint8  ServoClock_GetFractionalDividerRegister(void);

#define ServoClock_Enable()                         ServoClock_Start()
#define ServoClock_Disable()                        ServoClock_Stop()
#define ServoClock_SetDividerRegister(clkDivider, reset)  \
    ServoClock_SetFractionalDividerRegister((clkDivider), 0u)
#define ServoClock_SetDivider(clkDivider)           ServoClock_SetDividerRegister((clkDivider), 1u)
#define ServoClock_SetDividerValue(clkDivider)      ServoClock_SetDividerRegister((clkDivider) - 1u, 1u)


/***************************************
*             Registers
***************************************/
#if defined CYREG_PERI_DIV_CMD

#define ServoClock_DIV_ID     ServoClock__DIV_ID

#define ServoClock_CMD_REG    (*(reg32 *)CYREG_PERI_DIV_CMD)
#define ServoClock_CTRL_REG   (*(reg32 *)ServoClock__CTRL_REGISTER)
#define ServoClock_DIV_REG    (*(reg32 *)ServoClock__DIV_REGISTER)

#define ServoClock_CMD_DIV_SHIFT          (0u)
#define ServoClock_CMD_PA_DIV_SHIFT       (8u)
#define ServoClock_CMD_DISABLE_SHIFT      (30u)
#define ServoClock_CMD_ENABLE_SHIFT       (31u)

#define ServoClock_CMD_DISABLE_MASK       ((uint32)((uint32)1u << ServoClock_CMD_DISABLE_SHIFT))
#define ServoClock_CMD_ENABLE_MASK        ((uint32)((uint32)1u << ServoClock_CMD_ENABLE_SHIFT))

#define ServoClock_DIV_FRAC_MASK  (0x000000F8u)
#define ServoClock_DIV_FRAC_SHIFT (3u)
#define ServoClock_DIV_INT_MASK   (0xFFFFFF00u)
#define ServoClock_DIV_INT_SHIFT  (8u)

#else 

#define ServoClock_DIV_REG        (*(reg32 *)ServoClock__REGISTER)
#define ServoClock_ENABLE_REG     ServoClock_DIV_REG
#define ServoClock_DIV_FRAC_MASK  ServoClock__FRAC_MASK
#define ServoClock_DIV_FRAC_SHIFT (16u)
#define ServoClock_DIV_INT_MASK   ServoClock__DIVIDER_MASK
#define ServoClock_DIV_INT_SHIFT  (0u)

#endif/* CYREG_PERI_DIV_CMD */

#endif /* !defined(CY_CLOCK_ServoClock_H) */

/* [] END OF FILE */
