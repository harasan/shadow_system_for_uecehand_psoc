/*******************************************************************************
* File Name: AdcSarSeq_intClock.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_AdcSarSeq_intClock_H)
#define CY_CLOCK_AdcSarSeq_intClock_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
*        Function Prototypes
***************************************/
#if defined CYREG_PERI_DIV_CMD

void AdcSarSeq_intClock_StartEx(uint32 alignClkDiv);
#define AdcSarSeq_intClock_Start() \
    AdcSarSeq_intClock_StartEx(AdcSarSeq_intClock__PA_DIV_ID)

#else

void AdcSarSeq_intClock_Start(void);

#endif/* CYREG_PERI_DIV_CMD */

void AdcSarSeq_intClock_Stop(void);

void AdcSarSeq_intClock_SetFractionalDividerRegister(uint16 clkDivider, uint8 clkFractional);

uint16 AdcSarSeq_intClock_GetDividerRegister(void);
uint8  AdcSarSeq_intClock_GetFractionalDividerRegister(void);

#define AdcSarSeq_intClock_Enable()                         AdcSarSeq_intClock_Start()
#define AdcSarSeq_intClock_Disable()                        AdcSarSeq_intClock_Stop()
#define AdcSarSeq_intClock_SetDividerRegister(clkDivider, reset)  \
    AdcSarSeq_intClock_SetFractionalDividerRegister((clkDivider), 0u)
#define AdcSarSeq_intClock_SetDivider(clkDivider)           AdcSarSeq_intClock_SetDividerRegister((clkDivider), 1u)
#define AdcSarSeq_intClock_SetDividerValue(clkDivider)      AdcSarSeq_intClock_SetDividerRegister((clkDivider) - 1u, 1u)


/***************************************
*             Registers
***************************************/
#if defined CYREG_PERI_DIV_CMD

#define AdcSarSeq_intClock_DIV_ID     AdcSarSeq_intClock__DIV_ID

#define AdcSarSeq_intClock_CMD_REG    (*(reg32 *)CYREG_PERI_DIV_CMD)
#define AdcSarSeq_intClock_CTRL_REG   (*(reg32 *)AdcSarSeq_intClock__CTRL_REGISTER)
#define AdcSarSeq_intClock_DIV_REG    (*(reg32 *)AdcSarSeq_intClock__DIV_REGISTER)

#define AdcSarSeq_intClock_CMD_DIV_SHIFT          (0u)
#define AdcSarSeq_intClock_CMD_PA_DIV_SHIFT       (8u)
#define AdcSarSeq_intClock_CMD_DISABLE_SHIFT      (30u)
#define AdcSarSeq_intClock_CMD_ENABLE_SHIFT       (31u)

#define AdcSarSeq_intClock_CMD_DISABLE_MASK       ((uint32)((uint32)1u << AdcSarSeq_intClock_CMD_DISABLE_SHIFT))
#define AdcSarSeq_intClock_CMD_ENABLE_MASK        ((uint32)((uint32)1u << AdcSarSeq_intClock_CMD_ENABLE_SHIFT))

#define AdcSarSeq_intClock_DIV_FRAC_MASK  (0x000000F8u)
#define AdcSarSeq_intClock_DIV_FRAC_SHIFT (3u)
#define AdcSarSeq_intClock_DIV_INT_MASK   (0xFFFFFF00u)
#define AdcSarSeq_intClock_DIV_INT_SHIFT  (8u)

#else 

#define AdcSarSeq_intClock_DIV_REG        (*(reg32 *)AdcSarSeq_intClock__REGISTER)
#define AdcSarSeq_intClock_ENABLE_REG     AdcSarSeq_intClock_DIV_REG
#define AdcSarSeq_intClock_DIV_FRAC_MASK  AdcSarSeq_intClock__FRAC_MASK
#define AdcSarSeq_intClock_DIV_FRAC_SHIFT (16u)
#define AdcSarSeq_intClock_DIV_INT_MASK   AdcSarSeq_intClock__DIVIDER_MASK
#define AdcSarSeq_intClock_DIV_INT_SHIFT  (0u)

#endif/* CYREG_PERI_DIV_CMD */

#endif /* !defined(CY_CLOCK_AdcSarSeq_intClock_H) */

/* [] END OF FILE */
